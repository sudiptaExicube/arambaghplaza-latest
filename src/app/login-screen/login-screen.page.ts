import { Component, OnInit } from '@angular/core';
import { Router ,NavigationExtras} from '@angular/router';
import { GlobalService } from '../services/global.service';
import { MenuController, Platform, NavController } from '@ionic/angular';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-login-screen',
  templateUrl: './login-screen.page.html',
  styleUrls: ['./login-screen.page.scss'],
})
export class LoginScreenPage implements OnInit {
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';
  subscription:any;
  // public email: any = "7908250424";
  // public password: any = "123456";
  public email: any = "";
  public password: any = "";


  constructor(
    public global: GlobalService,
    private route: Router,
    private menu: MenuController,
    private apiService: ApiService,
    private platform: Platform,
    public navCtrl:NavController,
    public menuCtrl: MenuController
  ) {
    
  }

  ngOnInit() {
    this.menuCtrl.enable(false);
    this.menuCtrl.swipeGesture(false)
  }

 
  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribeWithPriority(9999, () => {
      // do nothing
    })
  }
  
  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  play() {
    var song = new Audio();
    song.src = 'assets/imgs/welcome-back.mp3';
    song.play();
  }

  hideShowPassword() {
    
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    console.log(this.passwordType)
    console.log(this.passwordIcon)
  }

  gotoSignUp() {
    this.route.navigate(['./signup-screen']);
  }

  isvalidphoneandEmailFormat(input) {
    var re = /^(?:\d{10}|\w+@\w+\.\w{2,3})$/;
    return re.test(input);
  }


  goLogin() {
  
    if(!this.email){
      this.global.presentToast("Please enter mobile number");
    }
    // else if(!this.isvalidphoneandEmailFormat(this.email)){
    //   this.global.presentToast("Invalid Email or Mobile Format");
    // }
    else if(!this.password){
      this.global.presentToast("Password field cannot be blank");
    }else{
      this.global.presentLoadingDefault();
      let data={
        email:this.email,
        password:this.password,
        fcm_token:this.global.deviceToken ? this.global.deviceToken :""
      }
      this.apiService.signin(data)
      .then((success:any)=>{
        this.global.presentLoadingClose()
        if(success.status == 'success'){
          if(success.data){
            let successdata:any = success.data; 
            localStorage.setItem("access_token",successdata.token.access_token)
            localStorage.setItem("user_details",JSON.stringify(successdata.user))
            this.global.userdetails = successdata.user;
            this.global.selectedScheme = successdata?.scheme;
            this.global.user_token = successdata.token.access_token;
            this.play()
            // this.route.navigate(['./main-dashboard']);
            this.navCtrl.navigateRoot(['./main-dashboard']);
            this.email = "";
            this.password = "";
          }
        }else if(success.status == "otpverification"){
          let navigationExtras: NavigationExtras = {
            queryParams: {
              otp_token:success?.data?.otp_token,
              from:"login"
            }
          }
          this.navCtrl.navigateRoot(['./otp-screen'],navigationExtras);
          this.global.presentToast(success.message);
        }else{
          this.global.presentToast(success.message);
        }
        console.log("Login success is  : ", success)
      })
      .catch((error:any)=>{
        this.global.presentLoadingClose()
        console.log("Login error : ", error)
      })
    }
    
  }

  storeData(res) {

  }
  gotoforgotPassword() {
    this.route.navigate(['./forgot-password']);
  }
}
