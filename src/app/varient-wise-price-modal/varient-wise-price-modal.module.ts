import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VarientWisePriceModalPageRoutingModule } from './varient-wise-price-modal-routing.module';

import { VarientWisePriceModalPage } from './varient-wise-price-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VarientWisePriceModalPageRoutingModule
  ],
  declarations: [VarientWisePriceModalPage]
})
export class VarientWisePriceModalPageModule {}
