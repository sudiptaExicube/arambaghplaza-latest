import { Component, OnInit } from '@angular/core';
import { NavParams, PopoverController } from '@ionic/angular';
@Component({
  selector: 'app-varient-wise-price-modal',
  templateUrl: './varient-wise-price-modal.page.html',
  styleUrls: ['./varient-wise-price-modal.page.scss'],
})
export class VarientWisePriceModalPage implements OnInit {
  public varientData:any;
  constructor(private popoverController: PopoverController, public navParams: NavParams,) {
    if(this.navParams.get('data')){
      console.log(this.navParams.get('data'))
      this.varientData = this.navParams.get('data')
    }

   }

  ngOnInit() {
  }

  coverNumber(data){
    return parseFloat(data)
  }
  closePopover(){
    this.popoverController.dismiss()
  }

}
