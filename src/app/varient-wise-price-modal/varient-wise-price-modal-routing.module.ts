import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VarientWisePriceModalPage } from './varient-wise-price-modal.page';

const routes: Routes = [
  {
    path: '',
    component: VarientWisePriceModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VarientWisePriceModalPageRoutingModule {}
