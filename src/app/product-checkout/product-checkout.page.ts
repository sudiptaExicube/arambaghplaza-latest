import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ModalController, NavController } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';
import { PromoCodesPage } from './../promo-codes/promo-codes.page';
import { CouponSuccessModalPage } from './../coupon-success-modal/coupon-success-modal.page';

declare var RazorpayCheckout:any;

@Component({
  selector: 'app-product-checkout',
  templateUrl: './product-checkout.page.html',
  styleUrls: ['./product-checkout.page.scss'],
})
export class ProductCheckoutPage implements OnInit {

  // public paymentoption: any = ""
  public paymentoption: any = 'online'

  public allCartData: any = {}
  public availableCoupon: any = [];
  public cartItems: any = [];

  public selectedCoupon: any = {};
  public selectedAddress_id: any = "";
  public selectedAddress: any = null;
  couponSuccess: boolean = false;

  public textboxCoupon: any = ""

  loading: boolean;
  constructor(
    private route: Router,
    public modalController: ModalController,
    public global: GlobalService,
    public apiService: ApiService,
    public navCtrl: NavController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.textboxCoupon = "";
    // this.global.presentLoadingDefault();

    /* === NEW LINE added ==== */
    this.couponSuccess = false;
    this.selectedCoupon = {};
    this.textboxCoupon = ""
    /* === NEW LINE added ==== */
    this.fetchCartList('skeliton');


  }

  refevent:any;
  doRefresh(event) {
    this.refevent = event;
    this.fetchCartList('pulltorefresh');
  }

  ionViewWillLeave() {
    this.textboxCoupon = "";
  }

  chnagePaymentMethod(value) {
    this.paymentoption = value;
  }

  coverNumber(data){
    return parseFloat(data)
  }

  public separateData() {
    console.log("this.selectedCoupon : ", this.selectedCoupon)
    // if (this.textboxCoupon != "" && this.allCartData?.coupon_discount == '0.00') {
    //   this.global.presentToast("Coupon code invalid");
    // }
    // else if(this.textboxCoupon!="" && this.allCartData?.coupon_discount != '0.00'){
    //   this.global.presentToast("Coupon code Applied");
    // }
    this.availableCoupon = this.allCartData?.available_coupons;
    this.cartItems = this.allCartData?.items;
    this.selectedCoupon = this.allCartData?.coupon_details;
    this.global.defaultAddress = this.allCartData?.default_address;
    if (this.allCartData?.default_address) {
      this.global.defaultAddress_id = this.allCartData?.default_address?.id
    }


  }

  //  Fetch cart list
  public fetchCartList(loadingType) {
    if (loadingType == 'loading') { this.global.presentLoadingDefault(); }
    if (loadingType == 'skeliton') { this.loading = true }
  
    /* === NEW LINE added ==== */
    if (loadingType == 'pulltorefresh') {
      this.couponSuccess = false;
      this.selectedCoupon = {};
      this.textboxCoupon = ""
    }
    /* === New line added ==== */

    let paramData = {
      address_id: "",
      coupon_code: this.selectedCoupon?.code
      // coupon_code: ''
    }
    console.log(paramData)
    this.apiService.fetchCartList(paramData)
      .then((success: any) => {
        console.log("Cart list : ", success)
        if (success.status == 'success') {
          // this.global.presentToast(success.message);
          if (success.data) {
            this.selectedAddress_id = success?.data?.default_address?.id;
            console.log(this.selectedAddress_id)
            if (loadingType == 'loading' && loadingType == "add2Cart") { this.global.presentLoadingClose() }
            this.allCartData = success.data;
            
            if (this.allCartData && this.couponSuccess) {
              this.openSuccessModal()
            }

            if (loadingType == 'skeliton') {
              setTimeout(() => {
                this.loading = false
              }, 1000)
            }

            if (loadingType == 'pulltorefresh') {
              this.refevent.target.complete()
            }

            
            if (this.allCartData) {
              this.separateData()
            }
          }
        }
        else if(success.status == 'error'){
          /* == NEW LINE aded ==== */
          this.couponSuccess = false;
          this.selectedCoupon = {};
          this.textboxCoupon = "";
          this.global.presentToast(success.message);
          /* == NEW LINE aded ==== */
          
          if (success.data) {
            this.selectedAddress_id = success?.data?.default_address?.id;
            console.log(this.selectedAddress_id)
            if (loadingType == 'loading' && loadingType == "add2Cart") { this.global.presentLoadingClose() }
            this.allCartData = success.data;
            /* ==  Code commented because of coupon code thrashold limit cross ====*/
            /*
            if (this.allCartData && this.couponSuccess) {
              this.openSuccessModal()
            }
            */

            if (loadingType == 'skeliton') {
              setTimeout(() => {
                this.loading = false
              }, 1000)
            }

            if (loadingType == 'pulltorefresh') {
              this.refevent.target.complete()
            }

            
            if (this.allCartData) {
              this.separateData()
            }
          }
          

        }
         else {
          if (loadingType == 'skeliton') {
            setTimeout(() => {
              this.loading = false
            }, 1000)
            if (success.status == 'error') {
              this.global.presentToast(success.message);
              this.selectedCoupon = "";
              this.textboxCoupon = "";
              this.couponSuccess = false;
              this.allCartData = success.data;
              if (this.allCartData) {
                this.separateData()
              }
            }
          }
          if (loadingType == 'loading' || loadingType == "add2Cart") {
            this.global.presentLoadingClose()
            this.global.presentToast(success.message);
          }

          if (loadingType == 'pulltorefresh') {
            this.refevent.target.complete()
          }

        }

      })
      .catch((error: any) => {
        if (loadingType == 'loader' && loadingType == "add2Cart") { this.global.presentLoadingClose() }
        console.log("Login error : ", error)
      })
  }

  // Cart add remove ===>
  public addToCart(item, value) {
    // this.showIncDescBtn = !this.showIncDescBtn
    this.couponSuccess=false;
    console.log(item)
    var itemPrice = item?.variant?.offeredprice;
    this.global.presentLoadingDefault();
    let paramData = {
      variant_id: item?.variant_id,
      quantity: value
    }
    this.apiService.addToCart(paramData)
      .then((success: any) => {
        console.log("Cart msg : ", success)
        if (success.status == 'success') {
          this.global.presentToast(success.message);
          if (success.data) {
            this.global.globalCartCount = success.data.cartcount;
            this.global.globalCartPrice = success.data.carttotal;
            if (value == -1) {
              if((this.allCartData?.item_total - itemPrice) < this.selectedCoupon?.min_order){
                this.selectedCoupon.code = null
                console.log((this.allCartData?.item_total - itemPrice))
              }
          //    this.selectedCoupon.code = parseFloat(this.selectedCoupon.min_order) > (parseFloat(this.allCartData?.item_total) - parseFloat(success.data.carttotal)) ? null : this.selectedCoupon.code
              
            }
          }
          this.global.presentLoadingClose()
          this.fetchCartList('add2Cart');
        } else {
          this.global.presentLoadingClose()
          this.global.presentToast(success.message);
        }
      })
      .catch((error: any) => {
        this.global.presentLoadingClose()
        console.log("Add to cart error : ", error)
      })
  }

  clearCoupon(){
    this.selectedCoupon = {};
    this.textboxCoupon = {};
    this.fetchCartList('skeliton');
    this.couponSuccess = false;
  }


  async gocouponPage() {
    
    console.log(this.availableCoupon);
    const modal = await this.modalController.create({
      component: PromoCodesPage,
      cssClass: 'my-custom-class',
      componentProps: {
        'coupon_codes': this.availableCoupon
      }
    });
    await modal.present();
    modal.onDidDismiss()
      .then((data: any) => {
        console.log(data);
        if (data?.data != null) {
          console.log(" JSON.parse(data.data) : ", JSON.parse(data.data))
          this.selectedCoupon = JSON.parse(data.data);
          this.textboxCoupon = JSON.parse(data.data);

          console.log("this.selectedCoupon : ", this.selectedCoupon)
          console.log("this.textboxCoupon : ", this.textboxCoupon)
          //this.global.presentLoadingDefault();
          this.fetchCartList('skeliton');
          this.couponSuccess = true;
        }else if(data?.data == null){

        } else {
          this.fetchCartList('skeliton')
        }

      });
      
  }


  async openSuccessModal() {
    
    const smodal = await this.modalController.create({
      component: CouponSuccessModalPage,
      cssClass: 'my-custom-class2',
      componentProps: { data: { discount: this.allCartData?.coupon_discount, couponCode: this.textboxCoupon?.code } }
    });
    await smodal.present();
    smodal.onDidDismiss()
      .then((data: any) => {
        console.log(data);

      });
  }
  chooseAddress() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        from_page: "checkout"
      }
    }
    this.route.navigate(['./address-list'], navigationExtras);
  }

  orderNow() {
    // let allitems = this.cartItems.items
    let allitems = this.cartItems;
    let found:boolean=false;
    for(let i=0; i<allitems.length; i++){
      if(allitems[i]){
        if(allitems[i].variant){
          if(allitems[i].variant.quantity == 0){
            found = true;
            break;
          }
        }
      }
    }
    if(found){
      this.global.presentToast("Some of your item is OUT OF STOCK. Please remove from cart and try again")
    }else{
      this.startOrdering()
    }


  }

  /* === Start ordering ==== */
  startOrdering(){
    console.log("order start")
    
    if (this.global.defaultAddress_id) {
      this.global.presentLoadingDefault();
      let paramData = {
        // address_id:this.selectedAddress_id ? this.selectedAddress_id : 1,
        address_id: this.global.defaultAddress_id,
        // payment_mode: "cash",
        // payment_mode: "online",
        payment_mode: this.paymentoption,
        coupon_code: this.selectedCoupon?.code
      }
      this.apiService.createNewOrder(paramData)
        .then((success: any) => {
          console.log("Cart msg : ", success)
          if (success.status == 'success') {
            this.global.presentLoadingClose();
            this.global.globalCartCount = 0;
            this.global.globalCartPrice = "";
            let navigationExtras: NavigationExtras = {
              queryParams: {
                order_code: success?.data?.order?.code
              }
            }
            this.navCtrl.navigateRoot(['./successfull-screen'], navigationExtras)
          
          }else if(success.status == 'error'){
            this.global.presentLoadingClose();
            this.global.presentToast(success.message);

          } else {
            this.global.presentLoadingClose();
            this.global.presentToast(success.message);
            if(success.status == "paymentinitiated"){
              if(success.data){
                if(success.data.rzpy_document){
                  console.log("success.data.rzpy_document : ", success.data.rzpy_document)
                  console.log("success.data : ", success.data)
                  this.startRazorpay(success.data.rzpy_document,success.data)
                }
              }
            }
          }

        })
        .catch((error: any) => {
          this.global.presentLoadingClose()
          console.log("Order now error : ", error)
        })
    } else {
      this.global.presentToast("Please select address first")
    }

  }


  startRazorpay(details,orderData){
    var options = {
      description: 'Pay online',
      image: 'https://arambaghplaza.com/public/favicon.png',
      currency: 'INR',
      // key:'rzp_live_K2PU2QUvfNlYpj',
      // key:'rzp_test_Wy1EiZs2Miv4bd',
      key:details?.key_id,
      order_id: details?.order_id,
      amount:details?.amount,
      name: "Arambagh Plaza Retail Store",
      prefill: {
        email: this.global.userdetails?.email,
        contact: this.global.userdetails.mobile,
        name: this.global.userdetails?.name
      },
      theme: {
        color: '#F37254'
      },
      modal: {
        ondismiss: function() {
          // alert('dismissed')
        }
      }
    };

    var successCallback = (success:any) => {
      console.log("payment success : ", success)
      var orderId = success.razorpay_order_id
      let paymentId = success.razorpay_payment_id;
      var signature = success.razorpay_signature
        if(success.razorpay_payment_id){
          this.paymentCallbackFunc(orderData,success.razorpay_payment_id)
        }


    }
    // .bind(this)
    var cancelCallback = function(error) {
      console.log("payment error : ", error)
      // alert(error.description);
      if(error.description){
        alert(error.description);
      }else if(error.error){
        if(error.error.description){
          alert(error.error.description);
        }
      }else{
        alert("Payment cancelled, please try again");
      }

    }.bind(this)
    RazorpayCheckout.on('payment.success', successCallback)
    RazorpayCheckout.on('payment.cancel', cancelCallback)
    RazorpayCheckout.open(options)
  }


  //Payment callback func
  paymentCallbackFunc(oDetails,razorpay_payment_id){
    // alert("hello")
    this.global.presentLoadingDefault();
    let paramData = {
      order_id:oDetails.order.id,
      razorpay_payment_id:razorpay_payment_id
    }
    // alert(JSON.stringify(paramData));
    this.apiService.paymentcallback(paramData)
      .then((success: any) => {
        console.log("paymentCallbackFunc success : ", success)
        if (success.status == 'success') {
          this.global.presentLoadingClose();
          // alert(JSON.stringify(success))
          this.global.globalCartCount = 0;
          this.global.globalCartPrice = "";
          let navigationExtras: NavigationExtras = {
            queryParams: {
              order_code: success?.data?.order?.code
            }
          }
          this.navCtrl.navigateRoot(['./successfull-screen'], navigationExtras)
        }else if (success.status == 'paymentfailed') {
          this.global.presentLoadingClose();
          // alert("paymentFailed : "+ JSON.stringify(success));
          this.global.globalCartCount = 0;
          this.global.globalCartPrice = "";
          let navigationExtras: NavigationExtras = {
            queryParams: {
              order_code: success?.data?.order?.code,
              message:success?.message
            }
          }
          this.navCtrl.navigateRoot(['./successfull-screen'], navigationExtras)

        }  else {
          this.global.presentLoadingClose();
          this.global.presentToast(success.message);
        }

      })
      .catch((error: any) => {
        this.global.presentLoadingClose()
        console.log("Order now error : ", error)
      })
  }


  gotoHome() {
    this.route.navigate(['./main-dashboard']);
  }


  addBulkQuantiry(item:any){
    console.log("working ....")
    this.global.alertController.create({
      header: 'Enter Quantity',
      message: 'Enter zero (0) to remove from cart',
      inputs: [
      {
          name: 'Quantity',
          type: 'number',
          placeholder: 'Enter Quantity',
          cssClass: 'change_design',
          // value:item?.product_variants[0]?.cart_quantity
          value:item?.variant?.cart_quantity
      }],
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Let me think');
          }
        },
        {
          text: 'Add',
          handler: (data) => {
            console.log("data : ", data.Quantity);
            if(data.Quantity > 99){
              this.global.presentToast("Quantity can not be grater than 99")
            }else{
              let count:number;
              // if(item?.product_variants[0]?.cart_quantity > data.Quantity){
              if(item?.variant?.cart_quantity > data.Quantity){
                // count = -(item?.product_variants[0]?.cart_quantity - data.Quantity)
                count = -(item?.variant?.cart_quantity - data.Quantity)
              }else{
                // count = data.Quantity - item?.product_variants[0]?.cart_quantity;
                count = data.Quantity - item?.variant?.cart_quantity;
              }
              console.log("working : ", count);
              this.addToCart(item, count)
              // this.addToCart(item, data.Quantity)
            }
          }
        }
      ]
    }).then(res => {
      res.present();
    });
  }

}
