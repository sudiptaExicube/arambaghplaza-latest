import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { ActionSheetController, MenuController, AlertController } from '@ionic/angular';
import { GlobalService } from './../services/global.service';
import { ApiService } from './../services/api.service';

@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.page.html',
  styleUrls: ['./my-orders.page.scss'],
})
export class MyOrdersPage implements OnInit {
  orderList: any;
  loading: boolean = true;
  cartCount: any;
  filterName: any = "all";
  constructor(
    private route: Router,
    public ActionSheetController: ActionSheetController,
    public global: GlobalService,
    private menu: MenuController,
    private apiService: ApiService,
    public alertController: AlertController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.orderHistoryList()
  }

  gotoCart() {
    this.route.navigate(['./notifications']);
  }

  orderHistoryList() {
    this.apiService.apiWithToken('order/fetch')
      .then((success: any) => {
        if (success.status == 'success') {
          console.log(success)
          let masterData = success.data;
          this.orderList = masterData?.orders;
          setTimeout(() => {
            this.loading = false
          }, 1000)
        } else {
          this.global.presentToast(success.message);
          setTimeout(() => {
            this.loading = false
          }, 1000)
        }

      }).catch((err: any) => {
        console.log(err);
        this.global.presentToast(err.message);
        setTimeout(() => {
          this.loading = false
        }, 1000)
      })
  }

  FilterData(status: any, event) {
    console.log(status)
    this.filterName = status
    if (status) {
      this.loading = true;
      let data = {
        status: status == 'all' ? "" : status
      }
      this.apiService.apiWithTokenBody(data, 'order/fetch')
        .then((success: any) => {
          if (success.status == 'success') {
            console.log(success)
            let masterData = success.data;
            this.orderList = masterData?.orders;
            if (event != null) { event.target.complete(); }
            setTimeout(() => {
              this.loading = false
            }, 1000)

          } else {
            this.global.presentToast(success.message);
            if (event != null) { event.target.complete(); }
            setTimeout(() => {
              this.loading = false
            }, 1000)

          }

        }).catch((err: any) => {
          console.log(err);
          this.global.presentToast(err.message);
          if (event != null) { event.target.complete(); }
          setTimeout(() => {
            this.loading = false
          }, 1000)
        })
    }
  }

  viewOrderDetails(item) {
    console.log(item)
    let navigationExtras: NavigationExtras = {
      queryParams: {
        order_id: item.id
      }
    };
    this.route.navigate(['./order-details'], navigationExtras);

  }

  async filterOrder() {
    const actionSheet = await this.ActionSheetController.create({
      header: 'PICK ONE ACTION',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'View All',
        icon: 'options-outline',
        handler: () => {
          this.FilterData("all", null)
        }
      }, {
        text: 'Wait For Approval',
        icon: 'options-outline',
        handler: () => {
          this.FilterData('received', null)
        }
      }, {
        text: 'Order Accepted',
        icon: 'options-outline',
        handler: () => {
          this.FilterData('accepted', null)
        }
      }, {
        text: 'Order In-Progress',
        icon: 'options-outline',
        handler: () => {
          this.FilterData('intransit', null)
        }
      }, {
        text: 'Out for Delivery',
        icon: 'options-outline',
        handler: () => {
          this.FilterData('outfordelivery', null)
        }
      }, {
        text: 'Delivered',
        icon: 'options-outline',
        handler: () => {
          this.FilterData('delivered', null)
        }
      }, {
        text: 'Cancel',
        icon: 'options-outline',
        // role: 'cancel',
        handler: () => {
          this.FilterData('cancelled', null)
        }
      }]
    });
    await actionSheet.present();

    const { role } = await actionSheet.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }


  doRefresh(event) {
    this.FilterData(this.filterName, event)
  }


  gotoHome() {
    this.route.navigate(['./main-dashboard']);
  }

  cancelOrderConfirmation(order) {
    this.alertController.create({
      header: 'Confirm Alert',
      message: 'Are you sure? you want to cancel order?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Let me think');
          }
        },
        {
          text: 'Yes!',
          handler: () => {
            if (order.payment_mode == 'online') {
              if (order.status == 'received' || order?.status == 'intransit' || order?.status == 'accepted') {
                let navigationExtras: NavigationExtras = {
                  queryParams: {
                    order_id: order.code,
                  }
                }
                this.route.navigate(['./support'], navigationExtras);
              } else {
                this.global.presentToast("Sorry!!! We are unable to cancel your order.Please call to Store")
              }
            } else {
              if (order.status == 'received') {
                this.global.presentLoadingDefault();
                this.apiService.apiWithTokenBody({ order_id: order?.id }, 'order/cancel-submit')
                  .then((success: any) => {
                    if (success.status == 'success') {
                      console.log(success)
                      this.global.presentLoadingClose()
                      this.global.presentToast(success.message);
                      this.orderHistoryList()
                    } else {
                      this.global.presentLoadingClose()
                      this.global.presentToast(success.message);
                      this.orderHistoryList()
                    }

                  }).catch((err: any) => {
                    this.global.presentLoadingClose()
                    this.global.presentToast(err.message);
                  })
              } else if (order?.status == 'intransit' || order?.status == 'accepted') {
                let navigationExtras: NavigationExtras = {
                  queryParams: {
                    order_id: order.code,
                  }
                }
                this.route.navigate(['./support'], navigationExtras);
              } else if (order?.status == 'cancelled') {
                this.global.presentToast("This order is already a cancelled")
              } else {
                this.global.presentToast("Sorry! We are unable to cancel your order for this scenario.Please call to store for better help")
              }
            }

          }
        }
      ]
    }).then(res => {
      res.present();
    });
  }

  replaceOrderConfirmation(order) {
    this.alertController.create({
      header: 'Confirm Alert',
      message: 'Are you sure? you want to replace order?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Let me think');
          }
        },
        {
          text: 'Yes!',
          handler: () => {
            let navigationExtras: NavigationExtras = {
              queryParams: {
                order_id: order.code,
              }
            }
            this.route.navigate(['./support'], navigationExtras);
          }
        }
      ]
    }).then(res => {
      res.present();
    });
  }

  getCartCount() {
    this.apiService.homePage('cart/count')
      .then((success: any) => {
        if (success.status == 'success') {
          console.log(success)
          let masterData = success.data;
          this.cartCount = masterData?.cartcount;
        } else {
          this.global.presentToast(success.message);
        }

      }).catch((err: any) => {
        console.log(err);
        this.global.presentToast(err.message);
      })
  }
}
