import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SignupScreenPage } from './signup-screen.page';

const routes: Routes = [
  {
    path: '',
    component: SignupScreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SignupScreenPageRoutingModule {}
