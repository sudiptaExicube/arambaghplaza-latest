import { Component, OnInit } from '@angular/core';
import { Router,NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-signup-screen',
  templateUrl: './signup-screen.page.html',
  styleUrls: ['./signup-screen.page.scss'],
})
export class SignupScreenPage implements OnInit {
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

  con_passwordType: string = 'password';
  con_passwordIcon: string = 'eye-off';


  public full_name:any="";
  public email:any="";
  public mobile_number:any="";
  public password:any="";
  public confirm_password:any="";


  constructor(
    public global: GlobalService,
    private route: Router,
    public apiService:ApiService,
    public navCtrl:NavController,

  ) { }

  ngOnInit() {
  }

  hideShowPassword(type) {
    if(type == 'password'){
      this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
      this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
      console.log(this.passwordType)
      console.log(this.passwordIcon)
    }else{
      this.con_passwordType = this.con_passwordType === 'text' ? 'password' : 'text';
      this.con_passwordIcon = this.con_passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
      console.log(this.con_passwordType)
      console.log(this.con_passwordIcon)
    }
  }

  gotoSignin(){
    // this.route.navigate(['./login-screen']);
    this.navCtrl.pop();
  }

  isvalidEmailFormat(email) {
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}

	isvalidphoneFormat(phone) {
		var re = /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[6789]\d{9}$/;
		return re.test(phone);
	}

  charecterValidation(text) {
    var charRegex = /^([a-zA-Z]+\s)*[a-zA-Z]+$/
    return charRegex.test(text);
  }

  signupFunction(){
    // this.route.navigate(['./otp-screen']);
    if(!this.full_name){
      this.global.presentToast("Full name field cannot be blank");
    }else if (!this.charecterValidation(this.full_name)) {
      this.global.presentToast("Please enter A-Z characters for full name");
    }
    // else if(!this.email){
    //   this.global.presentToast("Email field cannot be blank")
    // }
    else if ( this.email != "" && !this.isvalidEmailFormat(this.email)) {
			this.global.presentToast("Please enter valid email");
		}
    else if(!this.mobile_number){
      this.global.presentToast("Mobile number field cannot be blank")
    }else if(this.mobile_number.length<10){
      this.global.presentToast("Mobile number should be 10 digit")
    }else if (!this.isvalidphoneFormat(this.mobile_number)) {
      this.global.presentToast("Mobile number should start with 6, 7, 8 or 9")
		}else if(!this.password){
      this.global.presentToast("Password field cannot be blank")
    }else if(this.password.length<6){
      this.global.presentToast("Password should be minimum 6 digit")
    }else if(!this.confirm_password){
      this.global.presentToast("Confirm password field cannot be blank")
    }else{
      if(this.password != this.confirm_password){
        this.global.presentToast("Password and Confirm password doesn't match")
      }else{
        this.global.presentLoadingDefault();
        let data={
          name:this.full_name,
          email:this.email,
          mobile:this.mobile_number,
          password:this.password,
          password_confirmation:this.confirm_password,
          fcm_token:this.global.deviceToken ? this.global.deviceToken :""
        }
        this.apiService.signup(data)
        .then((success:any)=>{
          console.log(success)
          this.global.presentLoadingClose();
          if(success.status == 'success'){
            this.global.presentToast(success.message);
            this.navCtrl.navigateRoot(['./login-screen']);
          }else if(success.status == "otpverification"){
            this.global.presentToast("OTP sent to your registered email, please varify");
            let navigationExtras: NavigationExtras = {
              queryParams: {
                otp_token:success?.data?.otp_token,
                from:"registration"
              }
            }
            this.navCtrl.navigateRoot(['./otp-screen'],navigationExtras);
            this.global.presentToast(success.message);
          }else{
            this.global.presentToast(success.message);
          }
        })
        .catch((error:any)=>{
          this.global.presentLoadingClose()
          console.log("Signup error : ", error)
        })


      }


    }
  }

}

