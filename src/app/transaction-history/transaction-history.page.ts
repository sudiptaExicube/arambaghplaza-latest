import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { CustomerOrderDetailsPropoverPage } from '../customer-order-details-propover/customer-order-details-propover.page';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-transaction-history',
  templateUrl: './transaction-history.page.html',
  styleUrls: ['./transaction-history.page.scss'],
})
export class TransactionHistoryPage implements OnInit {

  purchaseHistoryArr: any = [];
  loading: boolean = true;
  curPage: any;
  constructor(public global: GlobalService,
    public api: ApiService,
    public popoverController: PopoverController) {

  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.curPage = 1
    this.getList(null)
  }


  getList(event) {
    console.log('hi...')
    // this.global.presentLoadingDefault();
    this.api.transactionHistory('wallet/statement').then((res: any) => {
      console.log(res);

      if (res.status == 'success') {
        // this.global.presentLoadingClose();
        setTimeout(() => {
          this.loading = false;
          this.purchaseHistoryArr = res?.data?.statement;
        //  this.purchaseHistoryArr = [
        //   {
        //     "id": 16,
        //     "user_id": 3,
        //     "ref_id": 2,
        //     "wallet_type": "mainwallet",
        //     "balance": "62.00",
        //     "trans_type": "credit",
        //     "amount": "3.00",
        //     "remarks": "Payment credited for Order No APZ-6735800160",
        //     "service": "downlineorder",
        //     "created_at": "24 Apr 22 - 03:26 AM",
        //     "updated_at": "24 Apr 22 - 03:26 AM",
        //     "orderdet": {
        //       "id": 2,
        //       "shop_id": 1,
        //       "user_id": 8,
        //       "code": "APZ-6735800160",
        //       "cust_name": "Sudipta Mukherjee",
        //       "cust_mobile": "7908250424",
        //       "cust_latitude": "22.9359688",
        //       "cust_longitude": "87.6955519",
        //       "cust_location": "Ahilyabai Holkar Road, Ahilyabai Holkar Road, Barul, Hooghly, West Bengal, 712602, India, IN",
        //       "cust_address": {
        //         "address_line1": "Bengai",
        //         "address_line2": "laxmitala",
        //         "postal_code": "712611",
        //         "city": null,
        //         "state": null,
        //         "country": null,
        //         "landmark": "bengai gram"
        //       },
        //       "item_total": 80,
        //       "delivery_charge": 20,
        //       "coupon_discount": 0,
        //       "payable_amount": 100,
        //       "adminprofit": 0,
        //       "status": "paymentinitiated",
        //       "expected_delivery_date": null,
        //       "status_log": [
        //         {
        //           "key": "paymentinitiated",
        //           "label": "Payment Initiated",
        //           "timestamp": "2022-01-06T22:15:28.012424Z"
        //         },
        //         {
        //           "key": "paymentfailed",
        //           "label": "Payment Failed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "received",
        //           "label": "Placed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "accepted",
        //           "label": "Accepted",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "intransit",
        //           "label": "In-Progress",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "outfordelivery",
        //           "label": "Out for Delivery",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "delivered",
        //           "label": "Delivered",
        //           "timestamp": null
        //         }
        //       ],
        //       "coupon_id": null,
        //       "payment_mode": "online",
        //       "payment_txnid": null,
        //       "rzpy_orderid": "order_IgcMFvMJjcew7Q",
        //       "rzpy_payid": null,
        //       "deliveryboy_id": null,
        //       "rating": null,
        //       "review": null,
        //       "admin_message": null,
        //       "revertcount": 0,
        //       "agent_id": null,
        //       "agent_commission": 0,
        //       "created_at": "07 Jan 22 - 09:15 AM",
        //       "updated_at": "16 Apr 22 - 01:35 AM",
        //       "deleted_at": null,
        //       "invoice": null,
        //       "status_text": "Payment Initiated"
        //     },
        //     "payoutreq": null,
        //     "closing_bal": "65.00"
        //   },
        //   {
        //     "id": 15,
        //     "user_id": 3,
        //     "ref_id": 2,
        //     "wallet_type": "mainwallet",
        //     "balance": "59.00",
        //     "trans_type": "credit",
        //     "amount": "3.00",
        //     "remarks": "Payment credited for Order No APZ-6735800160",
        //     "service": "order",
        //     "created_at": "24 Apr 22 - 03:25 AM",
        //     "updated_at": "24 Apr 22 - 03:25 AM",
        //     "orderdet": {
        //       "id": 2,
        //       "shop_id": 1,
        //       "user_id": 8,
        //       "code": "APZ-6735800160",
        //       "cust_name": "Sudipta Mukherjee",
        //       "cust_mobile": "7908250424",
        //       "cust_latitude": "22.9359688",
        //       "cust_longitude": "87.6955519",
        //       "cust_location": "Ahilyabai Holkar Road, Ahilyabai Holkar Road, Barul, Hooghly, West Bengal, 712602, India, IN",
        //       "cust_address": {
        //         "address_line1": "Bengai",
        //         "address_line2": "laxmitala",
        //         "postal_code": "712611",
        //         "city": null,
        //         "state": null,
        //         "country": null,
        //         "landmark": "bengai gram"
        //       },
        //       "item_total": 80,
        //       "delivery_charge": 20,
        //       "coupon_discount": 0,
        //       "payable_amount": 100,
        //       "adminprofit": 0,
        //       "status": "paymentinitiated",
        //       "expected_delivery_date": null,
        //       "status_log": [
        //         {
        //           "key": "paymentinitiated",
        //           "label": "Payment Initiated",
        //           "timestamp": "2022-01-06T22:15:28.012424Z"
        //         },
        //         {
        //           "key": "paymentfailed",
        //           "label": "Payment Failed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "received",
        //           "label": "Placed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "accepted",
        //           "label": "Accepted",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "intransit",
        //           "label": "In-Progress",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "outfordelivery",
        //           "label": "Out for Delivery",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "delivered",
        //           "label": "Delivered",
        //           "timestamp": null
        //         }
        //       ],
        //       "coupon_id": null,
        //       "payment_mode": "online",
        //       "payment_txnid": null,
        //       "rzpy_orderid": "order_IgcMFvMJjcew7Q",
        //       "rzpy_payid": null,
        //       "deliveryboy_id": null,
        //       "rating": null,
        //       "review": null,
        //       "admin_message": null,
        //       "revertcount": 0,
        //       "agent_id": null,
        //       "agent_commission": 0,
        //       "created_at": "07 Jan 22 - 09:15 AM",
        //       "updated_at": "16 Apr 22 - 01:35 AM",
        //       "deleted_at": null,
        //       "invoice": null,
        //       "status_text": "Payment Initiated"
        //     },
        //     "payoutreq": null,
        //     "closing_bal": "62.00"
        //   },
        //   {
        //     "id": 14,
        //     "user_id": 3,
        //     "ref_id": 2,
        //     "wallet_type": "mainwallet",
        //     "balance": "56.00",
        //     "trans_type": "credit",
        //     "amount": "3.00",
        //     "remarks": "Payment credited for Order No APZ-6735800160",
        //     "service": "order",
        //     "created_at": "24 Apr 22 - 03:25 AM",
        //     "updated_at": "24 Apr 22 - 03:25 AM",
        //     "orderdet": {
        //       "id": 2,
        //       "shop_id": 1,
        //       "user_id": 8,
        //       "code": "APZ-6735800160",
        //       "cust_name": "Sudipta Mukherjee",
        //       "cust_mobile": "7908250424",
        //       "cust_latitude": "22.9359688",
        //       "cust_longitude": "87.6955519",
        //       "cust_location": "Ahilyabai Holkar Road, Ahilyabai Holkar Road, Barul, Hooghly, West Bengal, 712602, India, IN",
        //       "cust_address": {
        //         "address_line1": "Bengai",
        //         "address_line2": "laxmitala",
        //         "postal_code": "712611",
        //         "city": null,
        //         "state": null,
        //         "country": null,
        //         "landmark": "bengai gram"
        //       },
        //       "item_total": 80,
        //       "delivery_charge": 20,
        //       "coupon_discount": 0,
        //       "payable_amount": 100,
        //       "adminprofit": 0,
        //       "status": "paymentinitiated",
        //       "expected_delivery_date": null,
        //       "status_log": [
        //         {
        //           "key": "paymentinitiated",
        //           "label": "Payment Initiated",
        //           "timestamp": "2022-01-06T22:15:28.012424Z"
        //         },
        //         {
        //           "key": "paymentfailed",
        //           "label": "Payment Failed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "received",
        //           "label": "Placed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "accepted",
        //           "label": "Accepted",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "intransit",
        //           "label": "In-Progress",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "outfordelivery",
        //           "label": "Out for Delivery",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "delivered",
        //           "label": "Delivered",
        //           "timestamp": null
        //         }
        //       ],
        //       "coupon_id": null,
        //       "payment_mode": "online",
        //       "payment_txnid": null,
        //       "rzpy_orderid": "order_IgcMFvMJjcew7Q",
        //       "rzpy_payid": null,
        //       "deliveryboy_id": null,
        //       "rating": null,
        //       "review": null,
        //       "admin_message": null,
        //       "revertcount": 0,
        //       "agent_id": null,
        //       "agent_commission": 0,
        //       "created_at": "07 Jan 22 - 09:15 AM",
        //       "updated_at": "16 Apr 22 - 01:35 AM",
        //       "deleted_at": null,
        //       "invoice": null,
        //       "status_text": "Payment Initiated"
        //     },
        //     "payoutreq": null,
        //     "closing_bal": "59.00"
        //   },
        //   {
        //     "id": 13,
        //     "user_id": 3,
        //     "ref_id": 2,
        //     "wallet_type": "mainwallet",
        //     "balance": "53.00",
        //     "trans_type": "credit",
        //     "amount": "3.00",
        //     "remarks": "Payment credited for Order No APZ-6735800160",
        //     "service": "order",
        //     "created_at": "24 Apr 22 - 03:24 AM",
        //     "updated_at": "24 Apr 22 - 03:24 AM",
        //     "orderdet": {
        //       "id": 2,
        //       "shop_id": 1,
        //       "user_id": 8,
        //       "code": "APZ-6735800160",
        //       "cust_name": "Sudipta Mukherjee",
        //       "cust_mobile": "7908250424",
        //       "cust_latitude": "22.9359688",
        //       "cust_longitude": "87.6955519",
        //       "cust_location": "Ahilyabai Holkar Road, Ahilyabai Holkar Road, Barul, Hooghly, West Bengal, 712602, India, IN",
        //       "cust_address": {
        //         "address_line1": "Bengai",
        //         "address_line2": "laxmitala",
        //         "postal_code": "712611",
        //         "city": null,
        //         "state": null,
        //         "country": null,
        //         "landmark": "bengai gram"
        //       },
        //       "item_total": 80,
        //       "delivery_charge": 20,
        //       "coupon_discount": 0,
        //       "payable_amount": 100,
        //       "adminprofit": 0,
        //       "status": "paymentinitiated",
        //       "expected_delivery_date": null,
        //       "status_log": [
        //         {
        //           "key": "paymentinitiated",
        //           "label": "Payment Initiated",
        //           "timestamp": "2022-01-06T22:15:28.012424Z"
        //         },
        //         {
        //           "key": "paymentfailed",
        //           "label": "Payment Failed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "received",
        //           "label": "Placed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "accepted",
        //           "label": "Accepted",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "intransit",
        //           "label": "In-Progress",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "outfordelivery",
        //           "label": "Out for Delivery",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "delivered",
        //           "label": "Delivered",
        //           "timestamp": null
        //         }
        //       ],
        //       "coupon_id": null,
        //       "payment_mode": "online",
        //       "payment_txnid": null,
        //       "rzpy_orderid": "order_IgcMFvMJjcew7Q",
        //       "rzpy_payid": null,
        //       "deliveryboy_id": null,
        //       "rating": null,
        //       "review": null,
        //       "admin_message": null,
        //       "revertcount": 0,
        //       "agent_id": null,
        //       "agent_commission": 0,
        //       "created_at": "07 Jan 22 - 09:15 AM",
        //       "updated_at": "16 Apr 22 - 01:35 AM",
        //       "deleted_at": null,
        //       "invoice": null,
        //       "status_text": "Payment Initiated"
        //     },
        //     "payoutreq": null,
        //     "closing_bal": "56.00"
        //   },
        //   {
        //     "id": 12,
        //     "user_id": 3,
        //     "ref_id": 2,
        //     "wallet_type": "mainwallet",
        //     "balance": "50.00",
        //     "trans_type": "credit",
        //     "amount": "3.00",
        //     "remarks": "Payment credited for Order No APZ-6735800160",
        //     "service": "order",
        //     "created_at": "24 Apr 22 - 03:24 AM",
        //     "updated_at": "24 Apr 22 - 03:24 AM",
        //     "orderdet": {
        //       "id": 2,
        //       "shop_id": 1,
        //       "user_id": 8,
        //       "code": "APZ-6735800160",
        //       "cust_name": "Sudipta Mukherjee",
        //       "cust_mobile": "7908250424",
        //       "cust_latitude": "22.9359688",
        //       "cust_longitude": "87.6955519",
        //       "cust_location": "Ahilyabai Holkar Road, Ahilyabai Holkar Road, Barul, Hooghly, West Bengal, 712602, India, IN",
        //       "cust_address": {
        //         "address_line1": "Bengai",
        //         "address_line2": "laxmitala",
        //         "postal_code": "712611",
        //         "city": null,
        //         "state": null,
        //         "country": null,
        //         "landmark": "bengai gram"
        //       },
        //       "item_total": 80,
        //       "delivery_charge": 20,
        //       "coupon_discount": 0,
        //       "payable_amount": 100,
        //       "adminprofit": 0,
        //       "status": "paymentinitiated",
        //       "expected_delivery_date": null,
        //       "status_log": [
        //         {
        //           "key": "paymentinitiated",
        //           "label": "Payment Initiated",
        //           "timestamp": "2022-01-06T22:15:28.012424Z"
        //         },
        //         {
        //           "key": "paymentfailed",
        //           "label": "Payment Failed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "received",
        //           "label": "Placed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "accepted",
        //           "label": "Accepted",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "intransit",
        //           "label": "In-Progress",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "outfordelivery",
        //           "label": "Out for Delivery",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "delivered",
        //           "label": "Delivered",
        //           "timestamp": null
        //         }
        //       ],
        //       "coupon_id": null,
        //       "payment_mode": "online",
        //       "payment_txnid": null,
        //       "rzpy_orderid": "order_IgcMFvMJjcew7Q",
        //       "rzpy_payid": null,
        //       "deliveryboy_id": null,
        //       "rating": null,
        //       "review": null,
        //       "admin_message": null,
        //       "revertcount": 0,
        //       "agent_id": null,
        //       "agent_commission": 0,
        //       "created_at": "07 Jan 22 - 09:15 AM",
        //       "updated_at": "16 Apr 22 - 01:35 AM",
        //       "deleted_at": null,
        //       "invoice": null,
        //       "status_text": "Payment Initiated"
        //     },
        //     "payoutreq": null,
        //     "closing_bal": "53.00"
        //   },
        //   {
        //     "id": 11,
        //     "user_id": 3,
        //     "ref_id": 2,
        //     "wallet_type": "mainwallet",
        //     "balance": "47.00",
        //     "trans_type": "credit",
        //     "amount": "3.00",
        //     "remarks": "Payment credited for Order No APZ-6735800160",
        //     "service": "order",
        //     "created_at": "24 Apr 22 - 03:24 AM",
        //     "updated_at": "24 Apr 22 - 03:24 AM",
        //     "orderdet": {
        //       "id": 2,
        //       "shop_id": 1,
        //       "user_id": 8,
        //       "code": "APZ-6735800160",
        //       "cust_name": "Sudipta Mukherjee",
        //       "cust_mobile": "7908250424",
        //       "cust_latitude": "22.9359688",
        //       "cust_longitude": "87.6955519",
        //       "cust_location": "Ahilyabai Holkar Road, Ahilyabai Holkar Road, Barul, Hooghly, West Bengal, 712602, India, IN",
        //       "cust_address": {
        //         "address_line1": "Bengai",
        //         "address_line2": "laxmitala",
        //         "postal_code": "712611",
        //         "city": null,
        //         "state": null,
        //         "country": null,
        //         "landmark": "bengai gram"
        //       },
        //       "item_total": 80,
        //       "delivery_charge": 20,
        //       "coupon_discount": 0,
        //       "payable_amount": 100,
        //       "adminprofit": 0,
        //       "status": "paymentinitiated",
        //       "expected_delivery_date": null,
        //       "status_log": [
        //         {
        //           "key": "paymentinitiated",
        //           "label": "Payment Initiated",
        //           "timestamp": "2022-01-06T22:15:28.012424Z"
        //         },
        //         {
        //           "key": "paymentfailed",
        //           "label": "Payment Failed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "received",
        //           "label": "Placed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "accepted",
        //           "label": "Accepted",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "intransit",
        //           "label": "In-Progress",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "outfordelivery",
        //           "label": "Out for Delivery",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "delivered",
        //           "label": "Delivered",
        //           "timestamp": null
        //         }
        //       ],
        //       "coupon_id": null,
        //       "payment_mode": "online",
        //       "payment_txnid": null,
        //       "rzpy_orderid": "order_IgcMFvMJjcew7Q",
        //       "rzpy_payid": null,
        //       "deliveryboy_id": null,
        //       "rating": null,
        //       "review": null,
        //       "admin_message": null,
        //       "revertcount": 0,
        //       "agent_id": null,
        //       "agent_commission": 0,
        //       "created_at": "07 Jan 22 - 09:15 AM",
        //       "updated_at": "16 Apr 22 - 01:35 AM",
        //       "deleted_at": null,
        //       "invoice": null,
        //       "status_text": "Payment Initiated"
        //     },
        //     "payoutreq": null,
        //     "closing_bal": "50.00"
        //   },
        //   {
        //     "id": 10,
        //     "user_id": 3,
        //     "ref_id": 1,
        //     "wallet_type": "mainwallet",
        //     "balance": "44.00",
        //     "trans_type": "credit",
        //     "amount": "3.00",
        //     "remarks": "Payment credited for Order No APZ-1387654761",
        //     "service": "downlineorder",
        //     "created_at": "24 Apr 22 - 03:18 AM",
        //     "updated_at": "24 Apr 22 - 03:22 AM",
        //     "orderdet": {
        //       "id": 1,
        //       "shop_id": 1,
        //       "user_id": 8,
        //       "code": "APZ-1387654761",
        //       "cust_name": "Sudipta Mukherjee",
        //       "cust_mobile": "7908250424",
        //       "cust_latitude": "22.9359688",
        //       "cust_longitude": "87.6955519",
        //       "cust_location": "Ahilyabai Holkar Road, Ahilyabai Holkar Road, Barul, Hooghly, West Bengal, 712602, India, IN",
        //       "cust_address": {
        //         "address_line1": "Bengai",
        //         "address_line2": "laxmitala",
        //         "postal_code": "712611",
        //         "city": null,
        //         "state": null,
        //         "country": null,
        //         "landmark": "bengai gram"
        //       },
        //       "item_total": 80,
        //       "delivery_charge": 20,
        //       "coupon_discount": 0,
        //       "payable_amount": 100,
        //       "adminprofit": 0,
        //       "status": "paymentinitiated",
        //       "expected_delivery_date": null,
        //       "status_log": [
        //         {
        //           "key": "paymentinitiated",
        //           "label": "Payment Initiated",
        //           "timestamp": "2022-01-06T22:15:20.404459Z"
        //         },
        //         {
        //           "key": "paymentfailed",
        //           "label": "Payment Failed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "received",
        //           "label": "Placed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "accepted",
        //           "label": "Accepted",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "intransit",
        //           "label": "In-Progress",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "outfordelivery",
        //           "label": "Out for Delivery",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "delivered",
        //           "label": "Delivered",
        //           "timestamp": null
        //         }
        //       ],
        //       "coupon_id": null,
        //       "payment_mode": "online",
        //       "payment_txnid": null,
        //       "rzpy_orderid": "order_IgcM7bzwZP5ysI",
        //       "rzpy_payid": null,
        //       "deliveryboy_id": null,
        //       "rating": null,
        //       "review": null,
        //       "admin_message": null,
        //       "revertcount": 0,
        //       "agent_id": 3,
        //       "agent_commission": 0,
        //       "created_at": "07 Jan 22 - 09:15 AM",
        //       "updated_at": "01 May 22 - 01:32 AM",
        //       "deleted_at": null,
        //       "invoice": null,
        //       "status_text": "Payment Initiated"
        //     },
        //     "payoutreq": null,
        //     "closing_bal": "47.00"
        //   },
        //   {
        //     "id": 9,
        //     "user_id": 3,
        //     "ref_id": 1,
        //     "wallet_type": "mainwallet",
        //     "balance": "94.00",
        //     "trans_type": "debit",
        //     "amount": "50.00",
        //     "remarks": "Payout Request APZ-1677197492 has been accepted and dispatched to your Bank Account",
        //     "service": "request",
        //     "created_at": "16 Apr 22 - 02:46 AM",
        //     "updated_at": "16 Apr 22 - 02:46 AM",
        //     "orderdet": null,
        //     "payoutreq": {
        //       "id": 1,
        //       "user_id": 3,
        //       "code": "APZ-1677197492",
        //       "wallet_type": "mainwallet",
        //       "amount": 50,
        //       "remarks": "Test Purpose",
        //       "status": "approved",
        //       "adminremarks": "TxnID : TTTTT10011524154",
        //       "type": "payout",
        //       "transaction_copy": null,
        //       "created_at": "16 Apr 22 - 02:36 AM",
        //       "updated_at": "16 Apr 22 - 02:46 AM",
        //       "transaction_copy_path": null
        //     },
        //     "closing_bal": "44.00"
        //   },
        //   {
        //     "id": 8,
        //     "user_id": 3,
        //     "ref_id": null,
        //     "wallet_type": "mainwallet",
        //     "balance": "124.00",
        //     "trans_type": "debit",
        //     "amount": "30.00",
        //     "remarks": "For Testing Purpose",
        //     "service": "fundreturn",
        //     "created_at": "16 Apr 22 - 02:29 AM",
        //     "updated_at": "16 Apr 22 - 02:29 AM",
        //     "orderdet": null,
        //     "payoutreq": null,
        //     "closing_bal": "94.00"
        //   },
        //   {
        //     "id": 7,
        //     "user_id": 3,
        //     "ref_id": null,
        //     "wallet_type": "mainwallet",
        //     "balance": "24.00",
        //     "trans_type": "credit",
        //     "amount": "100.00",
        //     "remarks": "For Testing Purpose",
        //     "service": "fundtransfer",
        //     "created_at": "16 Apr 22 - 02:29 AM",
        //     "updated_at": "16 Apr 22 - 02:29 AM",
        //     "orderdet": null,
        //     "payoutreq": null,
        //     "closing_bal": "124.00"
        //   },
        //   {
        //     "id": 6,
        //     "user_id": 3,
        //     "ref_id": 1,
        //     "wallet_type": "mainwallet",
        //     "balance": "21.00",
        //     "trans_type": "credit",
        //     "amount": "3.00",
        //     "remarks": "Payment credited for Order No APZ-1387654761",
        //     "service": "downlineorder",
        //     "created_at": "16 Apr 22 - 02:01 AM",
        //     "updated_at": "24 Apr 22 - 03:22 AM",
        //     "orderdet": {
        //       "id": 1,
        //       "shop_id": 1,
        //       "user_id": 8,
        //       "code": "APZ-1387654761",
        //       "cust_name": "Sudipta Mukherjee",
        //       "cust_mobile": "7908250424",
        //       "cust_latitude": "22.9359688",
        //       "cust_longitude": "87.6955519",
        //       "cust_location": "Ahilyabai Holkar Road, Ahilyabai Holkar Road, Barul, Hooghly, West Bengal, 712602, India, IN",
        //       "cust_address": {
        //         "address_line1": "Bengai",
        //         "address_line2": "laxmitala",
        //         "postal_code": "712611",
        //         "city": null,
        //         "state": null,
        //         "country": null,
        //         "landmark": "bengai gram"
        //       },
        //       "item_total": 80,
        //       "delivery_charge": 20,
        //       "coupon_discount": 0,
        //       "payable_amount": 100,
        //       "adminprofit": 0,
        //       "status": "paymentinitiated",
        //       "expected_delivery_date": null,
        //       "status_log": [
        //         {
        //           "key": "paymentinitiated",
        //           "label": "Payment Initiated",
        //           "timestamp": "2022-01-06T22:15:20.404459Z"
        //         },
        //         {
        //           "key": "paymentfailed",
        //           "label": "Payment Failed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "received",
        //           "label": "Placed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "accepted",
        //           "label": "Accepted",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "intransit",
        //           "label": "In-Progress",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "outfordelivery",
        //           "label": "Out for Delivery",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "delivered",
        //           "label": "Delivered",
        //           "timestamp": null
        //         }
        //       ],
        //       "coupon_id": null,
        //       "payment_mode": "online",
        //       "payment_txnid": null,
        //       "rzpy_orderid": "order_IgcM7bzwZP5ysI",
        //       "rzpy_payid": null,
        //       "deliveryboy_id": null,
        //       "rating": null,
        //       "review": null,
        //       "admin_message": null,
        //       "revertcount": 0,
        //       "agent_id": 3,
        //       "agent_commission": 0,
        //       "created_at": "07 Jan 22 - 09:15 AM",
        //       "updated_at": "01 May 22 - 01:32 AM",
        //       "deleted_at": null,
        //       "invoice": null,
        //       "status_text": "Payment Initiated"
        //     },
        //     "payoutreq": null,
        //     "closing_bal": "24.00"
        //   },
        //   {
        //     "id": 5,
        //     "user_id": 3,
        //     "ref_id": 1,
        //     "wallet_type": "mainwallet",
        //     "balance": "18.00",
        //     "trans_type": "credit",
        //     "amount": "3.00",
        //     "remarks": "Payment credited for Order No APZ-1387654761",
        //     "service": "downlineorder",
        //     "created_at": "16 Apr 22 - 02:01 AM",
        //     "updated_at": "24 Apr 22 - 03:22 AM",
        //     "orderdet": {
        //       "id": 1,
        //       "shop_id": 1,
        //       "user_id": 8,
        //       "code": "APZ-1387654761",
        //       "cust_name": "Sudipta Mukherjee",
        //       "cust_mobile": "7908250424",
        //       "cust_latitude": "22.9359688",
        //       "cust_longitude": "87.6955519",
        //       "cust_location": "Ahilyabai Holkar Road, Ahilyabai Holkar Road, Barul, Hooghly, West Bengal, 712602, India, IN",
        //       "cust_address": {
        //         "address_line1": "Bengai",
        //         "address_line2": "laxmitala",
        //         "postal_code": "712611",
        //         "city": null,
        //         "state": null,
        //         "country": null,
        //         "landmark": "bengai gram"
        //       },
        //       "item_total": 80,
        //       "delivery_charge": 20,
        //       "coupon_discount": 0,
        //       "payable_amount": 100,
        //       "adminprofit": 0,
        //       "status": "paymentinitiated",
        //       "expected_delivery_date": null,
        //       "status_log": [
        //         {
        //           "key": "paymentinitiated",
        //           "label": "Payment Initiated",
        //           "timestamp": "2022-01-06T22:15:20.404459Z"
        //         },
        //         {
        //           "key": "paymentfailed",
        //           "label": "Payment Failed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "received",
        //           "label": "Placed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "accepted",
        //           "label": "Accepted",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "intransit",
        //           "label": "In-Progress",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "outfordelivery",
        //           "label": "Out for Delivery",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "delivered",
        //           "label": "Delivered",
        //           "timestamp": null
        //         }
        //       ],
        //       "coupon_id": null,
        //       "payment_mode": "online",
        //       "payment_txnid": null,
        //       "rzpy_orderid": "order_IgcM7bzwZP5ysI",
        //       "rzpy_payid": null,
        //       "deliveryboy_id": null,
        //       "rating": null,
        //       "review": null,
        //       "admin_message": null,
        //       "revertcount": 0,
        //       "agent_id": 3,
        //       "agent_commission": 0,
        //       "created_at": "07 Jan 22 - 09:15 AM",
        //       "updated_at": "01 May 22 - 01:32 AM",
        //       "deleted_at": null,
        //       "invoice": null,
        //       "status_text": "Payment Initiated"
        //     },
        //     "payoutreq": null,
        //     "closing_bal": "21.00"
        //   },
        //   {
        //     "id": 4,
        //     "user_id": 3,
        //     "ref_id": 1,
        //     "wallet_type": "mainwallet",
        //     "balance": "15.00",
        //     "trans_type": "credit",
        //     "amount": "3.00",
        //     "remarks": "Payment credited for Order No APZ-1387654761",
        //     "service": "downlineorder",
        //     "created_at": "16 Apr 22 - 02:00 AM",
        //     "updated_at": "24 Apr 22 - 03:22 AM",
        //     "orderdet": {
        //       "id": 1,
        //       "shop_id": 1,
        //       "user_id": 8,
        //       "code": "APZ-1387654761",
        //       "cust_name": "Sudipta Mukherjee",
        //       "cust_mobile": "7908250424",
        //       "cust_latitude": "22.9359688",
        //       "cust_longitude": "87.6955519",
        //       "cust_location": "Ahilyabai Holkar Road, Ahilyabai Holkar Road, Barul, Hooghly, West Bengal, 712602, India, IN",
        //       "cust_address": {
        //         "address_line1": "Bengai",
        //         "address_line2": "laxmitala",
        //         "postal_code": "712611",
        //         "city": null,
        //         "state": null,
        //         "country": null,
        //         "landmark": "bengai gram"
        //       },
        //       "item_total": 80,
        //       "delivery_charge": 20,
        //       "coupon_discount": 0,
        //       "payable_amount": 100,
        //       "adminprofit": 0,
        //       "status": "paymentinitiated",
        //       "expected_delivery_date": null,
        //       "status_log": [
        //         {
        //           "key": "paymentinitiated",
        //           "label": "Payment Initiated",
        //           "timestamp": "2022-01-06T22:15:20.404459Z"
        //         },
        //         {
        //           "key": "paymentfailed",
        //           "label": "Payment Failed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "received",
        //           "label": "Placed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "accepted",
        //           "label": "Accepted",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "intransit",
        //           "label": "In-Progress",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "outfordelivery",
        //           "label": "Out for Delivery",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "delivered",
        //           "label": "Delivered",
        //           "timestamp": null
        //         }
        //       ],
        //       "coupon_id": null,
        //       "payment_mode": "online",
        //       "payment_txnid": null,
        //       "rzpy_orderid": "order_IgcM7bzwZP5ysI",
        //       "rzpy_payid": null,
        //       "deliveryboy_id": null,
        //       "rating": null,
        //       "review": null,
        //       "admin_message": null,
        //       "revertcount": 0,
        //       "agent_id": 3,
        //       "agent_commission": 0,
        //       "created_at": "07 Jan 22 - 09:15 AM",
        //       "updated_at": "01 May 22 - 01:32 AM",
        //       "deleted_at": null,
        //       "invoice": null,
        //       "status_text": "Payment Initiated"
        //     },
        //     "payoutreq": null,
        //     "closing_bal": "18.00"
        //   },
        //   {
        //     "id": 3,
        //     "user_id": 3,
        //     "ref_id": 1,
        //     "wallet_type": "mainwallet",
        //     "balance": "12.00",
        //     "trans_type": "credit",
        //     "amount": "3.00",
        //     "remarks": "Payment credited for Order No APZ-1387654761",
        //     "service": "downlineorder",
        //     "created_at": "16 Apr 22 - 02:00 AM",
        //     "updated_at": "24 Apr 22 - 03:22 AM",
        //     "orderdet": {
        //       "id": 1,
        //       "shop_id": 1,
        //       "user_id": 8,
        //       "code": "APZ-1387654761",
        //       "cust_name": "Sudipta Mukherjee",
        //       "cust_mobile": "7908250424",
        //       "cust_latitude": "22.9359688",
        //       "cust_longitude": "87.6955519",
        //       "cust_location": "Ahilyabai Holkar Road, Ahilyabai Holkar Road, Barul, Hooghly, West Bengal, 712602, India, IN",
        //       "cust_address": {
        //         "address_line1": "Bengai",
        //         "address_line2": "laxmitala",
        //         "postal_code": "712611",
        //         "city": null,
        //         "state": null,
        //         "country": null,
        //         "landmark": "bengai gram"
        //       },
        //       "item_total": 80,
        //       "delivery_charge": 20,
        //       "coupon_discount": 0,
        //       "payable_amount": 100,
        //       "adminprofit": 0,
        //       "status": "paymentinitiated",
        //       "expected_delivery_date": null,
        //       "status_log": [
        //         {
        //           "key": "paymentinitiated",
        //           "label": "Payment Initiated",
        //           "timestamp": "2022-01-06T22:15:20.404459Z"
        //         },
        //         {
        //           "key": "paymentfailed",
        //           "label": "Payment Failed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "received",
        //           "label": "Placed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "accepted",
        //           "label": "Accepted",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "intransit",
        //           "label": "In-Progress",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "outfordelivery",
        //           "label": "Out for Delivery",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "delivered",
        //           "label": "Delivered",
        //           "timestamp": null
        //         }
        //       ],
        //       "coupon_id": null,
        //       "payment_mode": "online",
        //       "payment_txnid": null,
        //       "rzpy_orderid": "order_IgcM7bzwZP5ysI",
        //       "rzpy_payid": null,
        //       "deliveryboy_id": null,
        //       "rating": null,
        //       "review": null,
        //       "admin_message": null,
        //       "revertcount": 0,
        //       "agent_id": 3,
        //       "agent_commission": 0,
        //       "created_at": "07 Jan 22 - 09:15 AM",
        //       "updated_at": "01 May 22 - 01:32 AM",
        //       "deleted_at": null,
        //       "invoice": null,
        //       "status_text": "Payment Initiated"
        //     },
        //     "payoutreq": null,
        //     "closing_bal": "15.00"
        //   },
        //   {
        //     "id": 2,
        //     "user_id": 3,
        //     "ref_id": 1,
        //     "wallet_type": "mainwallet",
        //     "balance": "9.00",
        //     "trans_type": "credit",
        //     "amount": "3.00",
        //     "remarks": "Payment credited for Order No APZ-1387654761",
        //     "service": "downlineorder",
        //     "created_at": "16 Apr 22 - 01:59 AM",
        //     "updated_at": "24 Apr 22 - 03:22 AM",
        //     "orderdet": {
        //       "id": 1,
        //       "shop_id": 1,
        //       "user_id": 8,
        //       "code": "APZ-1387654761",
        //       "cust_name": "Sudipta Mukherjee",
        //       "cust_mobile": "7908250424",
        //       "cust_latitude": "22.9359688",
        //       "cust_longitude": "87.6955519",
        //       "cust_location": "Ahilyabai Holkar Road, Ahilyabai Holkar Road, Barul, Hooghly, West Bengal, 712602, India, IN",
        //       "cust_address": {
        //         "address_line1": "Bengai",
        //         "address_line2": "laxmitala",
        //         "postal_code": "712611",
        //         "city": null,
        //         "state": null,
        //         "country": null,
        //         "landmark": "bengai gram"
        //       },
        //       "item_total": 80,
        //       "delivery_charge": 20,
        //       "coupon_discount": 0,
        //       "payable_amount": 100,
        //       "adminprofit": 0,
        //       "status": "paymentinitiated",
        //       "expected_delivery_date": null,
        //       "status_log": [
        //         {
        //           "key": "paymentinitiated",
        //           "label": "Payment Initiated",
        //           "timestamp": "2022-01-06T22:15:20.404459Z"
        //         },
        //         {
        //           "key": "paymentfailed",
        //           "label": "Payment Failed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "received",
        //           "label": "Placed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "accepted",
        //           "label": "Accepted",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "intransit",
        //           "label": "In-Progress",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "outfordelivery",
        //           "label": "Out for Delivery",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "delivered",
        //           "label": "Delivered",
        //           "timestamp": null
        //         }
        //       ],
        //       "coupon_id": null,
        //       "payment_mode": "online",
        //       "payment_txnid": null,
        //       "rzpy_orderid": "order_IgcM7bzwZP5ysI",
        //       "rzpy_payid": null,
        //       "deliveryboy_id": null,
        //       "rating": null,
        //       "review": null,
        //       "admin_message": null,
        //       "revertcount": 0,
        //       "agent_id": 3,
        //       "agent_commission": 0,
        //       "created_at": "07 Jan 22 - 09:15 AM",
        //       "updated_at": "01 May 22 - 01:32 AM",
        //       "deleted_at": null,
        //       "invoice": null,
        //       "status_text": "Payment Initiated"
        //     },
        //     "payoutreq": null,
        //     "closing_bal": "12.00"
        //   },
        //   {
        //     "id": 1,
        //     "user_id": 3,
        //     "ref_id": 1,
        //     "wallet_type": "mainwallet",
        //     "balance": "6.00",
        //     "trans_type": "credit",
        //     "amount": "3.00",
        //     "remarks": "Payment credited for Order No APZ-1387654761",
        //     "service": "downlineorder",
        //     "created_at": "16 Apr 22 - 01:59 AM",
        //     "updated_at": "24 Apr 22 - 03:22 AM",
        //     "orderdet": {
        //       "id": 1,
        //       "shop_id": 1,
        //       "user_id": 8,
        //       "code": "APZ-1387654761",
        //       "cust_name": "Sudipta Mukherjee",
        //       "cust_mobile": "7908250424",
        //       "cust_latitude": "22.9359688",
        //       "cust_longitude": "87.6955519",
        //       "cust_location": "Ahilyabai Holkar Road, Ahilyabai Holkar Road, Barul, Hooghly, West Bengal, 712602, India, IN",
        //       "cust_address": {
        //         "address_line1": "Bengai",
        //         "address_line2": "laxmitala",
        //         "postal_code": "712611",
        //         "city": null,
        //         "state": null,
        //         "country": null,
        //         "landmark": "bengai gram"
        //       },
        //       "item_total": 80,
        //       "delivery_charge": 20,
        //       "coupon_discount": 0,
        //       "payable_amount": 100,
        //       "adminprofit": 0,
        //       "status": "paymentinitiated",
        //       "expected_delivery_date": null,
        //       "status_log": [
        //         {
        //           "key": "paymentinitiated",
        //           "label": "Payment Initiated",
        //           "timestamp": "2022-01-06T22:15:20.404459Z"
        //         },
        //         {
        //           "key": "paymentfailed",
        //           "label": "Payment Failed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "received",
        //           "label": "Placed",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "accepted",
        //           "label": "Accepted",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "intransit",
        //           "label": "In-Progress",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "outfordelivery",
        //           "label": "Out for Delivery",
        //           "timestamp": null
        //         },
        //         {
        //           "key": "delivered",
        //           "label": "Delivered",
        //           "timestamp": null
        //         }
        //       ],
        //       "coupon_id": null,
        //       "payment_mode": "online",
        //       "payment_txnid": null,
        //       "rzpy_orderid": "order_IgcM7bzwZP5ysI",
        //       "rzpy_payid": null,
        //       "deliveryboy_id": null,
        //       "rating": null,
        //       "review": null,
        //       "admin_message": null,
        //       "revertcount": 0,
        //       "agent_id": 3,
        //       "agent_commission": 0,
        //       "created_at": "07 Jan 22 - 09:15 AM",
        //       "updated_at": "01 May 22 - 01:32 AM",
        //       "deleted_at": null,
        //       "invoice": null,
        //       "status_text": "Payment Initiated"
        //     },
        //     "payoutreq": null,
        //     "closing_bal": "9.00"
        //   }
        // ]
          if (event != null) { event.target.complete(); }
          //this.global.presentToast(res.msg);
        }, 200)
      }else {
        this.global.presentToast(res.msg);
        // this.global.presentLoadingClose();
        setTimeout(() => {
          this.loading = false;
          this.purchaseHistoryArr = [];
          if (event != null) { event.target.complete(); }
        }, 200)
      }
    })
  }

  doRefresh(event) {
    this.getList(event)
  }

  async presentModal(data) {
    const popover = await this.popoverController.create({
      component: CustomerOrderDetailsPropoverPage,
      cssClass: 'contact-popover',
      translucent: true,
      componentProps: {data:data}
    });
    await popover.present();
  
    const { role } = await popover.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  
  }

}