import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EarningProgramPageRoutingModule } from './earning-program-routing.module';

import { EarningProgramPage } from './earning-program.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EarningProgramPageRoutingModule
  ],
  declarations: [EarningProgramPage]
})
export class EarningProgramPageModule {}
