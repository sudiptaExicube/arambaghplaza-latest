import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EarningProgramPage } from './earning-program.page';

const routes: Routes = [
  {
    path: '',
    component: EarningProgramPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EarningProgramPageRoutingModule {}
