import { Component, OnInit } from '@angular/core';
import { NavParams, PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-customer-order-details-propover',
  templateUrl: './customer-order-details-propover.page.html',
  styleUrls: ['./customer-order-details-propover.page.scss'],
})
export class CustomerOrderDetailsPropoverPage implements OnInit {

  details:any = "";
  constructor(public navParams: NavParams,private popoverController: PopoverController) { 
    if(this.navParams.get('data')){
      console.log(this.navParams.get('data'))
      this.details = this.navParams.get('data')
    }
  }

  ngOnInit() {
  }

  closePopover(){
    this.popoverController.dismiss()
  }
}
