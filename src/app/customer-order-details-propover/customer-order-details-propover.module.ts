import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CustomerOrderDetailsPropoverPageRoutingModule } from './customer-order-details-propover-routing.module';

import { CustomerOrderDetailsPropoverPage } from './customer-order-details-propover.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomerOrderDetailsPropoverPageRoutingModule
  ],
  declarations: [CustomerOrderDetailsPropoverPage]
})
export class CustomerOrderDetailsPropoverPageModule {}
