import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CustomerOrderDetailsPropoverPage } from './customer-order-details-propover.page';

describe('CustomerOrderDetailsPropoverPage', () => {
  let component: CustomerOrderDetailsPropoverPage;
  let fixture: ComponentFixture<CustomerOrderDetailsPropoverPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerOrderDetailsPropoverPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CustomerOrderDetailsPropoverPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
