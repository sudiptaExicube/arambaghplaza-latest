import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReplacementPolicyPage } from './replacement-policy.page';

const routes: Routes = [
  {
    path: '',
    component: ReplacementPolicyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReplacementPolicyPageRoutingModule {}
