import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-replacement-policy',
  templateUrl: './replacement-policy.page.html',
  styleUrls: ['./replacement-policy.page.scss'],
})
export class ReplacementPolicyPage implements OnInit {
  content:any;
  loading:boolean = true;
  constructor(public global: GlobalService,
    private apiService: ApiService,) { }

  ngOnInit() {
    this.getData()
  }
  getData() {
    this.apiService.apiWithoutToken({slug:'replacement-policy'},'cms/contents')
      .then((success: any) => {
        if (success.status == 'success') {
          console.log(success)
           this.content = success?.data?.cmscontent?.content;
           console.log(this.content)
          setTimeout(() => {
            this.loading = false
          }, 1000)
        } else {
          this.global.presentToast(success.message);
          setTimeout(() => {
            this.loading = false
          }, 1000)
        }

      }).catch((err: any) => {
        console.log(err);
        this.global.presentToast(err.message);
        setTimeout(() => {
          this.loading = false
        }, 1000)
      })
  }

}
