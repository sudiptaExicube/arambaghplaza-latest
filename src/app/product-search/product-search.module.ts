import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductSearchPageRoutingModule } from './product-search-routing.module';

import { ProductSearchPage } from './product-search.page';
import { LazyLoadImageModule } from 'ng-lazyload-image';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductSearchPageRoutingModule,
    LazyLoadImageModule
  ],
  declarations: [ProductSearchPage]
})
export class ProductSearchPageModule {}
