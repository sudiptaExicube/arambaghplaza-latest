import { Component, OnInit, Inject, ViewChildren,ViewChild, QueryList, OnDestroy, NgZone } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { GlobalService } from './../services/global.service';
import { IonSlide, IonSlides, MenuController, NavController } from '@ionic/angular';
import { ApiService } from './../services/api.service';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { IonRouterOutlet, Platform } from '@ionic/angular';
import { PopoverController } from '@ionic/angular';
import { VarientWisePriceModalPage } from '../varient-wise-price-modal/varient-wise-price-modal.page';


@Component({
  selector: 'app-main-dashboard',
  templateUrl: './main-dashboard.page.html',
  styleUrls: ['./main-dashboard.page.scss'],
})
export class MainDashboardPage implements OnInit {
  @ViewChild('slides', { read: IonSlides }) slides: IonSlides; //

  slideOpts = {
    initialSlide: 0,
    speed: 200,
    autoplay: true
  };

  cartAddEffect:boolean = false;
  public banner: any = []

  public discountImgs: any = [
    { url: 'assets/imgs/drink.jpg' },
    { url: 'assets/imgs/discount_two.jpeg' },
    { url: 'assets/imgs/discount_one.jpeg' },
    { url: 'assets/imgs/discount_two.jpeg' },
    { url: 'assets/imgs/discount_one.jpeg' }
  ]

  public startingOff: any = [];
  public advt:any = []
  public categoryList: any = [];
  public showIncDescBtn: boolean = false;

  //== NEW added ====
  public homePageAllData: any = {}
  // public allBanners:any={};
  public saleProducts: any = [];
  // public allFeatureCategory:any=[];

  maintenance_msg: any = "";
  public defaultImage:any = 'https://miro.medium.com/max/441/1*9EBHIOzhE1XfMYoKz1JcsQ.gif';

  backButtonSubscription;
  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;
  lastTimeBackPress = Date.now();
  timePeriodToExit = 2000;

  constructor(private route: Router,
    public global: GlobalService,
    private menu: MenuController,
    private apiService: ApiService,
    private loadingCtrl: LoadingController,
    public menuCtrl: MenuController,
    private platform: Platform,
    public navCtrl: NavController,
    public zone:NgZone,
    public popoverController: PopoverController,
  ) { }

  ngOnInit() {
    this.menuCtrl.enable(true);
    this.menuCtrl.swipeGesture(true)
  }

  ionViewWillEnter() {
    // console.log(this.global.userdetails)
    // console.log(this.global.user_token)
    this.cartCount();
    this.global.imageLoading();
    this.loadHomepageData(null,null);
    // this.checkVersion()
   
    this.platform.backButton.subscribeWithPriority(0, () => {
      if (this.route.url === '/main-dashboard') {
        navigator['app'].exitApp();
      }else{
        this.navCtrl.pop();
      }
      
      });
  }


  async viewComission(item) {
    
    const popover = await this.popoverController.create({
      component: VarientWisePriceModalPage,
      cssClass: 'offer-popover',
      translucent: true,
      componentProps: {data:item}
    });
    await popover.present();
  
    const { role } = await popover.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  
  }


  addBulkQuantiry(item:any){
    this.zone.run(()=>{
      this.global.alertController.create({
        header: 'Enter Quantity',
        message: 'Enter zero (0) to remove from cart',
        inputs: [
        {
            name: 'Quantity',
            type: 'number',
            placeholder: 'Enter Quantity',
            cssClass: 'change_design',
            value:item?.product_variants[0]?.cart_quantity
        }],
        buttons: [
          {
            text: 'Cancel',
            handler: () => {
              console.log('Let me think');
            }
          },
          {
            text: 'Add',
            handler: (data) => {
              console.log("data : ", data.Quantity);
              if(data.Quantity > 99){
                this.global.presentToast("Quantity can not be grater than 99")
              }else{
                let count:number;
                if(item?.product_variants[0]?.cart_quantity > data.Quantity){
                  count = -(item?.product_variants[0]?.cart_quantity - data.Quantity)
                }else{
                  count = data.Quantity - item?.product_variants[0]?.cart_quantity;
                }
                console.log("working : ", count);
                this.addToCart(item, count)
                // this.addToCart(item, data.Quantity)
              }
            }
          }
        ]
      }).then(res => {
        res.present();
      });
    })

  }

  checkVersion(){
    this.global.appVersion.getVersionNumber()
    .then((value:any)=>{
      console.log("Version value is : ", value);
      if(value != this.global.appversionValue){
          this.global.versionAlert("A new version is available. Please update app now")
          .then((success:any)=>{
            if(success == 1){
              navigator['app'].exitApp();
            }else if(success == 2){
              navigator['app'].exitApp();
              window.open("https://play.google.com/store/apps/details?id=com.techinvein.arambaghplaza","_system")
            }
          })
      }

    })
    .catch((error:any)=>{
      console.log("Version error is : ", error);
    })
  }

  




  /* === Slider movement control ==== */
  lastSlideCall() {
    this.slides.stopAutoplay();
  }
  initialSlideCall() {
    this.slides.startAutoplay();
  }
  /* === Slider movement control ==== */



  //Search product====>
  searchProduct() {
    this.route.navigate(['./product-search']);
  }



  public cartCount() {
    this.apiService.fetchCartCount()
      .then((success: any) => {
        console.log("Cart count : ", success)
        if (success.status == 'success') {
          if (success.data) {
            console.log("cart count data : ", success.data);
            this.global.globalCartCount = success.data.cartcount;
            this.global.globalCartPrice = success.data.carttotal;
          }
        } else {
        }

      })
      .catch((error: any) => {
        console.log("Cartcount error : ", error)
      })
  }

  public seperateSectionsFromAlData() {
    // this.allBanners = this.homePageAllData?.appbanners;
    this.saleProducts = this.homePageAllData?.sale_products
    // this.allFeatureCategory = this.homePageAllData?.featured_categories

    this.banner = this.homePageAllData?.appbanners?.top;
    this.startingOff = this.homePageAllData?.appbanners?.middle;
    this.advt = this.homePageAllData?.appbanners?.middle;
    this.categoryList = this.homePageAllData?.featured_categories;
    this.saleProducts = this.homePageAllData?.sale_products;
    this.maintenance_msg =  this.homePageAllData?.maintenance_msg

    if (this.maintenance_msg == "") {
      this.menuCtrl.enable(true);
      this.menuCtrl.swipeGesture(true)
    } else {
      this.menuCtrl.enable(false);
      this.menuCtrl.swipeGesture(false)
    }
  }

  public loadHomepageData(event,from) {
    // this.global.presentLoadingDefault();

    this.apiService.loadHomepageData()
      .then((success: any) => {
        this.global.presentLoadingClose()
        if (success.status == 'success') {
          if (success.data) {
            if (event != null) { event.target.complete(); }
            console.log("HOme page data : ", success.data);
            this.homePageAllData = success.data;
            if(this.global.appversionValue == ""){
              this.global.appversionValue = this.homePageAllData.app_version;  
              this.checkVersion()
            }

            if(from == 'addtocart'){
              this.cartAddEffect=true;
              setTimeout(()=>{
                this.cartAddEffect = false;
              },400)
            }
           
            
            this.global.store_details = this.homePageAllData?.contact_details
            if (this.homePageAllData) {
              this.seperateSectionsFromAlData()
            }
          }
        } else {
          this.global.presentToast(success.message);
          if (event != null) { event.target.complete(); }
        }
        console.log("Login success is  : ", success)
      })
      .catch((error: any) => {
        this.global.presentLoadingClose()
        console.log("Login error : ", error);
        if (event != null) { event.target.complete(); }
      })
  }

  doRefresh(event) {
    this.loadHomepageData(event,null);
    this.cartCount();
  }

  public addToCart(item, value) {
    
    if(item?.product_variants[0]?.qty_prices != '' && item?.product_variants[0]?.cart_quantity < 3){
      this.viewComission(item)
    }
    
    // this.showIncDescBtn = !this.showIncDescBtn
    this.global.presentLoadingDefault();
    let paramData = {
      variant_id: item?.product_variants[0]?.id,
      quantity: value
    }
    this.apiService.addToCart(paramData)
      .then((success: any) => {
        console.log("Cart data : ", success)
        if (success.status == 'success') {
          this.global.presentToast(success.message);
          if (success.data) {
            this.global.globalCartCount = success.data.cartcount;
            this.global.globalCartPrice = success.data.carttotal;
           
          }
          this.loadHomepageData(null,'addtocart');
        } else {
          this.global.presentLoadingClose()
          this.global.presentToast(success.message);
        }

      })
      .catch((error: any) => {
        this.global.presentLoadingClose()
        console.log("Login error : ", error)
      })


  }











  // public addToCart(){
  //   this.showIncDescBtn = !this.showIncDescBtn
  // }

  gotoCart() {
    this.route.navigate(['./product-checkout']);
  }

  openCategory() {
    this.route.navigate(['./category-list']);
  }

  productDetails(item) {
    //this.route.navigate(['./product-details']);
   // console.log(this.saleProducts)
   
    let navigationExtras: NavigationExtras = {
      queryParams: {
        from:"home",
        product_id: item.id,
        others_products: JSON.stringify(this.saleProducts)
      }
    };
    this.route.navigate(['./product-details'], navigationExtras);
  }

  // loadHome(){
  //   this.global.presentLoadingDefault()
  //   this.apiService.homePage('homepage/load')
  //   .then((success:any)=>{
  //     if(success.status == 'success'){
  //       console.log(success)
  //       let masterData = success.data;
  //       this.banner = masterData?.appbanners?.top;
  //       this.startingOff = masterData?.appbanners?.middle;
  //       this.categoryList = masterData?.featured_categories;
  //       this.saleProducts = masterData?.sale_products;
  //      // this.global.presentToast(success.message);
  //       this.global.presentLoadingClose()
  //     }else{
  //       this.global.presentToast(success.message);
  //       this.global.presentLoadingClose()
  //     }

  //   }).catch((err: any) => {
  //     console.log(err);
  //     this.global.presentToast(err.message);
  //     this.global.presentLoadingClose()
  //   })
  // }

  // getCartCount(){
  //   this.apiService.homePage('cart/count')
  //   .then((success:any)=>{
  //     if(success.status == 'success'){
  //       console.log(success)
  //       let masterData = success.data;
  //       this.cartCount = masterData?.cartcount;
  //     }else{
  //       this.global.presentToast(success.message);
  //     }

  //   }).catch((err: any) => {
  //     console.log(err);
  //     this.global.presentToast(err.message);
  //   })
  // }

  gotoSubCatPage(item) {
    this.global.selectedCategory = item.id;
    this.route.navigate(['./subcategory-list']);
    //this.route.navigate(['./product-list']);
  }
}
