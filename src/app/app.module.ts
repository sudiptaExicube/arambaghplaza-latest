import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';



import { HttpClient, HttpClientModule } from '@angular/common/http';
// import { CalenderModalPageModule } from './calender-modal/calender-modal.module';

import { Geolocation } from '@awesome-cordova-plugins/geolocation/ngx';
import { NativeGeocoder } from '@awesome-cordova-plugins/native-geocoder/ngx';

import { AndroidPermissions } from '@awesome-cordova-plugins/android-permissions/ngx';
import { LocationAccuracy } from '@awesome-cordova-plugins/location-accuracy/ngx';

import { File } from '@awesome-cordova-plugins/file/ngx';
import { DomSanitizer } from '@angular/platform-browser';
import { FilePath } from '@awesome-cordova-plugins/file-path/ngx';
import { OneSignal } from '@awesome-cordova-plugins/onesignal/ngx';
import { Diagnostic } from '@awesome-cordova-plugins/diagnostic/ngx';
import { CallNumber } from '@awesome-cordova-plugins/call-number/ngx';

import { AppVersion } from '@awesome-cordova-plugins/app-version/ngx';
// import { SmsRetriever } from '@awesome-cordova-plugins/sms-retriever';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';


@NgModule({
  declarations: [AppComponent],
  // imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule],
  // providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  // bootstrap: [AppComponent],

  imports: [
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    
    // CalenderModalPageModule,
    HttpClientModule,
  ],
  providers: [
    Geolocation,
    NativeGeocoder,
    AndroidPermissions,
    LocationAccuracy,
    // Camera,
    File,
    FilePath,
    // Base64,
    // PhotoViewer,
    OneSignal,
    Diagnostic,
    CallNumber,
    AppVersion,
    // SmsRetriever,
    SocialSharing,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent],




})
export class AppModule {}



/*

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CalenderModalPageModule } from './calender-modal/calender-modal.module';

import { Geolocation } from '@awesome-cordova-plugins/geolocation/ngx';
import { NativeGeocoder } from '@awesome-cordova-plugins/native-geocoder/ngx';

import { AndroidPermissions } from '@awesome-cordova-plugins/android-permissions/ngx';
import { LocationAccuracy } from '@awesome-cordova-plugins/location-accuracy/ngx';

import { File } from '@awesome-cordova-plugins/file/ngx';
import { DomSanitizer } from '@angular/platform-browser';
import { FilePath } from '@awesome-cordova-plugins/file-path/ngx';
import { OneSignal } from '@awesome-cordova-plugins/onesignal/ngx';
import { Diagnostic } from '@awesome-cordova-plugins/diagnostic/ngx';
import { CallNumber } from '@awesome-cordova-plugins/call-number/ngx';

import { AppVersion } from '@awesome-cordova-plugins/app-version/ngx';
// import { SmsRetriever } from '@awesome-cordova-plugins/sms-retriever';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    
    CalenderModalPageModule,
    HttpClientModule,
  ],
  providers: [
    Geolocation,
    NativeGeocoder,
    AndroidPermissions,
    LocationAccuracy,
    // Camera,
    File,
    FilePath,
    // Base64,
    // PhotoViewer,
    OneSignal,
    Diagnostic,
    CallNumber,
    AppVersion,
    // SmsRetriever,
    SocialSharing,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

*/