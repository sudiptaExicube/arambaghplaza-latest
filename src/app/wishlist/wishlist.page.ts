import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ApiService } from './../services/api.service';
import { MenuController } from '@ionic/angular';
import { GlobalService } from './../services/global.service';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.page.html',
  styleUrls: ['./wishlist.page.scss'],
})
export class WishlistPage implements OnInit {
  public defaultImage:any = 'https://miro.medium.com/max/441/1*9EBHIOzhE1XfMYoKz1JcsQ.gif';
  public subcategoryList:any=[]

  public segment:any;
  public showCount:boolean = false;
  public cartCount:number;
 
  public productList:any=[]
  public loading:boolean = true;

  public subCat_id:any="";
  constructor(
    private route: Router,
    public global: GlobalService,
    private menu: MenuController,
    private apiService: ApiService,
    public activeRoute: ActivatedRoute,
    ) { }

  ngOnInit() {
  

  }

  ionViewWillEnter() {
    this.getWishList('loader',null)
  }

  favFunction(index,value){
    this.global.presentLoadingDefault()
    let data = {
      product_id:this.productList[index]?.product_id
    }
    this.apiService.apiWithTokenBody(data,'wishlist/submit')
    .then((success:any)=>{
      if(success.status == 'success'){
        this.global.presentLoadingClose()
        console.log(success)
        this.productList[index].product.is_wishlist = value;
        this.global.presentToast(success.message);
        this.loading = true;
        this.getWishList('loader',null)
      }else{
        this.global.presentToast(success.message);
        this.global.presentLoadingClose()
      }
     
    }).catch((err: any) => {
      console.log(err);
      this.global.presentLoadingClose()
      this.global.presentToast(err.message);
     
    })
  }

  gotoCart(){
    this.route.navigate(['./product-checkout']);
  }


  public addProduct(index){
    this.productList[index].quantity = this.productList[index].quantity + 1; 
    // this.showCount = !this.showCount;
  }

  public addQuantity(index){
    this.productList[index].quantity = this.productList[index].quantity + 1; 
  }

  public removeQuantity(index){
    if(this.productList[index].quantity != 0){
      this.productList[index].quantity = this.productList[index].quantity - 1; 
    }
  }

  gotoProductDetails(item){
    // this.route.navigate(['./product-details']);
    let navigationExtras: NavigationExtras = {
      queryParams: {
        product_id: item?.product_id,
        others_products:JSON.stringify([])
      }
    };
    this.route.navigate(['./product-details'], navigationExtras);
  }
  

  getCartCount(){
    this.apiService.homePage('cart/count')
    .then((success:any)=>{
      if(success.status == 'success'){
        console.log(success)
        let masterData = success.data;
        this.cartCount = masterData?.cartcount;
      }else{
        this.global.presentToast(success.message);
      }
     
    }).catch((err: any) => {
      console.log(err);
      this.global.presentToast(err.message);
    })
  }

  getWishList(type,event){
    let data = {}
    this.apiService.apiWithTokenBody(data,'wishlist/list')
    .then((success:any)=>{
      if(success.status == 'success'){
        console.log(success)
        let masterData = success.data;
        this.productList = masterData.wishlists;
        console.log(this.productList)
        type == null ?this.global.presentLoadingClose():null
        if(event !=null){ event.target.complete();}
        this.getCartCount()
        setTimeout(()=>{
          this.loading=false
        },1000)
      }else{
        this.global.presentToast(success.message);
        if(event !=null){ event.target.complete();}
        setTimeout(()=>{
          this.loading=false
        },1000)
        type == null ?this.global.presentLoadingClose():null
      }
     
    }).catch((err: any) => {
      console.log(err);
      if(event !=null){ event.target.complete();}
      type == null ?this.global.presentLoadingClose():null
      this.global.presentToast(err.message);
      setTimeout(()=>{
        this.loading=false
      },1000)
    })
  }

  doRefresh(event) {
    this.getWishList(null,event)
  }

  addBulkQuantiry(item:any){
    console.log("working ....")
    this.global.alertController.create({
      header: 'Enter Quantity',
      message: 'Enter zero (0) to remove from cart',
      inputs: [
      {
          name: 'Quantity',
          type: 'number',
          placeholder: 'Enter Quantity',
          cssClass: 'change_design',
          // value:item?.product_variants[0]?.cart_quantity
          value:item?.product?.product_variants[0]?.cart_quantity
      }],
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Let me think');
          }
        },
        {
          text: 'Add',
          handler: (data) => {
            console.log("data : ", data.Quantity);
            if(data.Quantity > 99){
              this.global.presentToast("Quantity can not be grater than 99")
            }else{
              let count:number;
              // if(item?.product_variants[0]?.cart_quantity > data.Quantity){
              if(item?.product?.product_variants[0]?.cart_quantity > data.Quantity){
                // count = -(item?.product_variants[0]?.cart_quantity - data.Quantity)
                count = -(item?.product?.product_variants[0]?.cart_quantity - data.Quantity)
              }else{
                // count = data.Quantity - item?.product_variants[0]?.cart_quantity;
                count = data.Quantity - item?.product?.product_variants[0]?.cart_quantity;
              }
              console.log("working : ", count);
              this.addToCart(item, count)
              // this.addToCart(item, data.Quantity)
            }
          }
        }
      ]
    }).then(res => {
      res.present();
    });
  }
  
  public addToCart(item,value){
    // this.showIncDescBtn = !this.showIncDescBtn
    this.global.presentLoadingDefault();
   console.log(item)
    let paramData={
      variant_id: item?.product?.product_variants[0]?.id,
      quantity:value
    }
    this.apiService.addToCart(paramData)
      .then((success:any)=>{
        console.log("Cart msg : ", success)
        // this.global.presentLoadingClose();
        if(success.status == 'success'){
          this.global.presentToast(success.message);
          if(success.data){
            this.global.globalCartCount = success.data.cartcount;
            this.global.globalCartPrice = success.data.carttotal;
          }
           this.getWishList(null,null);
        //  this.loadSubCat_wiseProductList();
        }else{
          this.global.presentLoadingClose()
          this.global.presentToast(success.message);
        }
        
      })
      .catch((error:any)=>{
        this.global.presentLoadingClose()
        console.log("Login error : ", error)
      })


  }


}
