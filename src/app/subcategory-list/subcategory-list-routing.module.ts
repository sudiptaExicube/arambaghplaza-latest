import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubcategoryListPage } from './subcategory-list.page';

const routes: Routes = [
  {
    path: '',
    component: SubcategoryListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubcategoryListPageRoutingModule {}
