import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { GlobalService } from './../services/global.service';
import { MenuController } from '@ionic/angular';
import { ApiService } from './../services/api.service';
@Component({
  selector: 'app-subcategory-list',
  templateUrl: './subcategory-list.page.html',
  styleUrls: ['./subcategory-list.page.scss'],
})
export class SubcategoryListPage implements OnInit {
  cartCount:number;
  subcategoryList:any=[];
  public loading:boolean = true;
  public defaultImage:any = 'https://miro.medium.com/max/441/1*9EBHIOzhE1XfMYoKz1JcsQ.gif';
  constructor(
    private route: Router,
    public global: GlobalService,
    private menu: MenuController,
    private apiService: ApiService,) { 
      this.getAllCategory(null)
    }

  ngOnInit() {
  }
  ionViewWillEnter() {
    // this.getAllCategory(null)
  }
  gotoCart(){
    this.route.navigate(['./product-checkout']);
  }

  doRefresh(event){
    this.getAllCategory(event)
  }

  getAllCategory(event){
   // this.global.presentLoadingDefault();
    let data = {
      parent_id:this.global.selectedCategory
    }
    this.apiService.apiWithoutToken(data,'categories/fetch')
    .then((success:any)=>{
      if(success.status == 'success'){
        //this.global.presentLoadingClose()
        console.log(success)
        let masterData = success.data;
        this.subcategoryList = masterData.categories;
        if(event !=null){ event.target.complete();}
        setTimeout(()=>{
          this.loading=false
        },1000)
      }else{
        //this.global.presentLoadingClose()
        this.global.presentToast(success.message);
        setTimeout(()=>{
          this.loading=false
        },1000)
        if(event !=null){ event.target.complete();}
      }
     
    }).catch((err: any) => {
      console.log(err);
      //this.global.presentLoadingClose();
      setTimeout(()=>{
        this.loading=false
      },1000)
      this.global.presentToast(err.message);
      if(event !=null){ event.target.complete();}
    })
  }

  getCartCount(){
    this.apiService.homePage('cart/count')
    .then((success:any)=>{
      if(success.status == 'success'){
        console.log(success)
        let masterData = success.data;
        this.cartCount = masterData?.cartcount;
      }else{
        this.global.presentToast(success.message);
      }
     
    }).catch((err: any) => {
      console.log(err);
      this.global.presentToast(err.message);
    })
  }

  gotoSubCatPage(item){
    console.log("item : ", item);
    let navigationExtras: NavigationExtras = {
      queryParams: {
        subCat_id: item.id
      }
    };
    this.route.navigate(['./product-list'],navigationExtras);
  }

}
