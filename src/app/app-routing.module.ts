import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  // {
  //   path: '',
  //   redirectTo: 'folder/Inbox',
  //   pathMatch: 'full'
  // },
  {
    path: '',
    redirectTo: 'intro',
    pathMatch: 'full'
  },
  // {
  //   path: 'folder/:id',
  //   loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  // },
  {
    path: 'intro',
    loadChildren: () => import('./intro/intro.module').then( m => m.IntroPageModule)
  },
  {
    path: 'about-us',
    loadChildren: () => import('./about-us/about-us.module').then( m => m.AboutUsPageModule)
  },
  {
    path: 'add-address',
    loadChildren: () => import('./add-address/add-address.module').then( m => m.AddAddressPageModule)
  },
  {
    path: 'add-customer',
    loadChildren: () => import('./add-customer/add-customer.module').then( m => m.AddCustomerPageModule)
  },
  {
    path: 'address-list',
    loadChildren: () => import('./address-list/address-list.module').then( m => m.AddressListPageModule)
  },
  {
    path: 'address-modal',
    loadChildren: () => import('./address-modal/address-modal.module').then( m => m.AddressModalPageModule)
  },
  {
    path: 'calender-modal',
    loadChildren: () => import('./calender-modal/calender-modal.module').then( m => m.CalenderModalPageModule)
  },
  {
    path: 'category-list',
    loadChildren: () => import('./category-list/category-list.module').then( m => m.CategoryListPageModule)
  },
  {
    path: 'change-password',
    loadChildren: () => import('./change-password/change-password.module').then( m => m.ChangePasswordPageModule)
  },
  {
    path: 'coupon-success-modal',
    loadChildren: () => import('./coupon-success-modal/coupon-success-modal.module').then( m => m.CouponSuccessModalPageModule)
  },
  {
    path: 'customer-order-details-propover',
    loadChildren: () => import('./customer-order-details-propover/customer-order-details-propover.module').then( m => m.CustomerOrderDetailsPropoverPageModule)
  },
  {
    path: 'downline-members',
    loadChildren: () => import('./downline-members/downline-members.module').then( m => m.DownlineMembersPageModule)
  },
  {
    path: 'earning-program',
    loadChildren: () => import('./earning-program/earning-program.module').then( m => m.EarningProgramPageModule)
  },
  {
    path: 'edit-account',
    loadChildren: () => import('./edit-account/edit-account.module').then( m => m.EditAccountPageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
  },
  // {
  //   path: 'intro',
  //   loadChildren: () => import('./intro/intro.module').then( m => m.IntroPageModule)
  // },
  {
    path: 'know-more',
    loadChildren: () => import('./know-more/know-more.module').then( m => m.KnowMorePageModule)
  },
  {
    path: 'login-screen',
    loadChildren: () => import('./login-screen/login-screen.module').then( m => m.LoginScreenPageModule)
  },
  {
    path: 'main-dashboard',
    loadChildren: () => import('./main-dashboard/main-dashboard.module').then( m => m.MainDashboardPageModule)
  },
  {
    path: 'my-orders',
    loadChildren: () => import('./my-orders/my-orders.module').then( m => m.MyOrdersPageModule)
  },
  {
    path: 'notifications',
    loadChildren: () => import('./notifications/notifications.module').then( m => m.NotificationsPageModule)
  },
  {
    path: 'order-details',
    loadChildren: () => import('./order-details/order-details.module').then( m => m.OrderDetailsPageModule)
  },
  {
    path: 'otp-screen',
    loadChildren: () => import('./otp-screen/otp-screen.module').then( m => m.OtpScreenPageModule)
  },
  {
    path: 'payment-success',
    loadChildren: () => import('./payment-success/payment-success.module').then( m => m.PaymentSuccessPageModule)
  },
  {
    path: 'payout-request',
    loadChildren: () => import('./payout-request/payout-request.module').then( m => m.PayoutRequestPageModule)
  },
  {
    path: 'privacy-policy',
    loadChildren: () => import('./privacy-policy/privacy-policy.module').then( m => m.PrivacyPolicyPageModule)
  },
  {
    path: 'product-checkout',
    loadChildren: () => import('./product-checkout/product-checkout.module').then( m => m.ProductCheckoutPageModule)
  },
  {
    path: 'product-details',
    loadChildren: () => import('./product-details/product-details.module').then( m => m.ProductDetailsPageModule)
  },
  {
    path: 'product-list',
    loadChildren: () => import('./product-list/product-list.module').then( m => m.ProductListPageModule)
  },
  {
    path: 'product-search',
    loadChildren: () => import('./product-search/product-search.module').then( m => m.ProductSearchPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'promo-codes',
    loadChildren: () => import('./promo-codes/promo-codes.module').then( m => m.PromoCodesPageModule)
  },
  {
    path: 'rating-page',
    loadChildren: () => import('./rating-page/rating-page.module').then( m => m.RatingPagePageModule)
  },
  {
    path: 'replacement-policy',
    loadChildren: () => import('./replacement-policy/replacement-policy.module').then( m => m.ReplacementPolicyPageModule)
  },
  {
    path: 'signup-screen',
    loadChildren: () => import('./signup-screen/signup-screen.module').then( m => m.SignupScreenPageModule)
  },
  {
    path: 'subcategory-list',
    loadChildren: () => import('./subcategory-list/subcategory-list.module').then( m => m.SubcategoryListPageModule)
  },
  {
    path: 'successfull-screen',
    loadChildren: () => import('./successfull-screen/successfull-screen.module').then( m => m.SuccessfullScreenPageModule)
  },
  {
    path: 'support',
    loadChildren: () => import('./support/support.module').then( m => m.SupportPageModule)
  },
  {
    path: 'support-list',
    loadChildren: () => import('./support-list/support-list.module').then( m => m.SupportListPageModule)
  },
  {
    path: 'terms-page',
    loadChildren: () => import('./terms-page/terms-page.module').then( m => m.TermsPagePageModule)
  },
  {
    path: 'transaction-history',
    loadChildren: () => import('./transaction-history/transaction-history.module').then( m => m.TransactionHistoryPageModule)
  },
  {
    path: 'wallet',
    loadChildren: () => import('./wallet/wallet.module').then( m => m.WalletPageModule)
  },
  {
    path: 'wishlist',
    loadChildren: () => import('./wishlist/wishlist.module').then( m => m.WishlistPageModule)
  },
  {
    path: 'varient-wise-price-modal',
    loadChildren: () => import('./varient-wise-price-modal/varient-wise-price-modal.module').then( m => m.VarientWisePriceModalPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
