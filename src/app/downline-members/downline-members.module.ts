import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DownlineMembersPageRoutingModule } from './downline-members-routing.module';

import { DownlineMembersPage } from './downline-members.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DownlineMembersPageRoutingModule
  ],
  declarations: [DownlineMembersPage]
})
export class DownlineMembersPageModule {}
