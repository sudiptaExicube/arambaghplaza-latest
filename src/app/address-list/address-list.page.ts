import { Component, OnInit } from '@angular/core';
import { ActionSheetController, MenuController, NavController } from '@ionic/angular';
import { Router,NavigationExtras, ActivatedRoute } from '@angular/router';
import { GlobalService } from './../services/global.service';
import { ApiService } from './../services/api.service';
@Component({
  selector: 'app-address-list',
  templateUrl: './address-list.page.html',
  styleUrls: ['./address-list.page.scss'],
})
export class AddressListPage implements OnInit {
  public addresses:any=[];
  loading:boolean = true;

  public navParamData:any=""

  constructor(
    public actionSheetController: ActionSheetController,
    private route: Router,
    public global: GlobalService,
    private menu: MenuController,
    private apiService: ApiService,
    public activeRoute: ActivatedRoute,
    public navCtrl:NavController
    ) { }

  ngOnInit() {
    this.activeRoute.queryParams.subscribe(params => {
      console.log("params : ", params)
      if(params["from_page"]){
        this.navParamData = params["from_page"]
      }
      // this.resturant_id = params["restaurantId"];
    })
  }

  ionViewWillEnter() {
    this.getAllAddress()
  }
  
  presentActionSheet(data:any) {
    if(this.navParamData == "checkout"){
      this.openActionShit_with_NavParam(data)
    }else{
      this.openActionShit_withOut_NavParam(data)
    }

  }

  async openActionShit_with_NavParam(data:any){
    const actionSheet = await this.actionSheetController.create({
      header: 'PICK ONE ACTION',
      cssClass: 'my-custom-class',
      buttons: [  {
        text: 'Deliver to this address',
        icon: 'create-outline',
        handler: () => {
            console.log("openActionShit_with_NavParam : ", data);
            this.setAddress_toDefault(data);
        }
      }, {
        text: 'Edit Address',
        icon: 'create-outline',
        handler: () => {
          let navigationExtras: NavigationExtras = {
            // queryParams: data
            queryParams:{
              details:JSON.stringify(data)
            }
          }
        
          this.route.navigate(['./add-address'],navigationExtras);
        }
      },{
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
          //this.navCtrl.pop();
        }
      }]
    });
    await actionSheet.present();

    const { role } = await actionSheet.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  async openActionShit_withOut_NavParam(data:any){
    const actionSheet = await this.actionSheetController.create({
      header: 'PICK ONE ACTION',
      cssClass: 'my-custom-class',
      buttons: [  {
        text: 'Mark as default address',
        icon: 'map-outline',
        handler: () => {
          this.setAddress_toDefault(data)
        }
      },{
        text: 'Edit Address',
        icon: 'create-outline',
        handler: () => {
          let navigationExtras: NavigationExtras = {
            // queryParams: data
            queryParams:{
              details:JSON.stringify(data)
            }
          }
        
          this.route.navigate(['./add-address'],navigationExtras);
        }
      },{
        text: 'Delete address',
        icon: 'trash',
        handler: () => {
          this.deleteAddress(data)
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();

    const { role } = await actionSheet.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  // Mark as default address
  setAddress_toDefault(data:any){
    this.global.presentLoadingDefault()
    console.log(data);
    let paramData={
      id:data?.id
    }
    this.apiService.setDefaultAddress(paramData)
      .then((success:any)=>{
        this.global.presentLoadingClose();
        if(success.status == 'success'){
          this.global.presentToast(success.message);
          //OLD CODE
          // this.getAllAddress()
          
          /* New COde ====  */
          if(this.navParamData == "checkout"){
            this.navCtrl.pop();
          }else{
            this.getAllAddress()
          }
          /* New COde ====  */

        }else{
          this.global.presentToast(success.message);
        }
        
      })
      .catch((error:any)=>{
        this.global.presentLoadingClose()
        console.log("Login error : ", error)
      })

  }

  //== DELETE address ===
  deleteAddress(data:any){
    this.global.presentLoadingDefault()
    console.log(data);
    let paramData={
      id:data?.id
    }
    this.apiService.deleteSavedAddress(paramData)
      .then((success:any)=>{
        this.global.presentLoadingClose();
        console.log("success : ", success);
        if(success.status == 'success'){
          this.global.presentToast(success.message);
          this.navCtrl.pop();
        }else{
          this.global.presentToast(success.message);
        }
        
      })
      .catch((error:any)=>{
        this.global.presentLoadingClose()
        console.log("Login error : ", error)
      })
  }

  getAllAddress(){
    //this.global.presentLoadingDefault();
    this.apiService.apiWithToken('user/address/fetch')
    .then((success:any)=>{
      console.log(success)
      if(success.status == 'success'){
        console.log(success)
        this.addresses = success?.data?.addresses;
        //this.global.presentLoadingClose()
        setTimeout(()=>{
          this.loading=false
        },1000)
      }else{
        this.global.presentToast(success.message);
        //this.global.presentLoadingClose();
        setTimeout(()=>{
          this.loading=false
        },1000)
      }
     
    }).catch((err: any) => {
      console.log(err);
      this.global.presentToast(err.message);
      //this.global.presentLoadingClose();
      setTimeout(()=>{
        this.loading=false
      },1000)
     
    })
  }

  addNewAddress(){
    let navigationExtras: NavigationExtras = {
      // queryParams: data
      queryParams:{
        fromPage:'newaddress'
      }
    }
    this.route.navigate(['./add-address'],navigationExtras);
  }

}
