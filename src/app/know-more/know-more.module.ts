import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule, NavParams} from '@ionic/angular';

import { KnowMorePageRoutingModule } from './know-more-routing.module';

import { KnowMorePage } from './know-more.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    
    KnowMorePageRoutingModule
  ],
  declarations: [KnowMorePage]
})
export class KnowMorePageModule {}
