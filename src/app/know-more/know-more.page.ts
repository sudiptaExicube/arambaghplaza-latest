import { Component, OnInit } from '@angular/core';
import { NavParams, PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-know-more',
  templateUrl: './know-more.page.html',
  styleUrls: ['./know-more.page.scss'],
})
export class KnowMorePage implements OnInit {
  comissionDetails:any = "";
  constructor(public navParams: NavParams,private popoverController: PopoverController) { 
    if(this.navParams.get('data')){
      console.log(this.navParams.get('data'))
      this.comissionDetails = this.navParams.get('data')
    }
  }

  ngOnInit() {
  }
  closePopover(){
    this.popoverController.dismiss()
  }

}
