import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-payout-request',
  templateUrl: './payout-request.page.html',
  styleUrls: ['./payout-request.page.scss'],
})
export class PayoutRequestPage implements OnInit {

  payoutRequestArr: any = [];
  loading: boolean = true;
  curPage: any;
  constructor(public global: GlobalService,
    public api: ApiService) {

  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.curPage = 1
    this.getList(null)
  }


  getList(event) {
    console.log('hi...')
    // this.global.presentLoadingDefault();
    this.api.transactionHistory('wallet/payout/request/fetch').then((res: any) => {
      console.log(res);
      if (res.status == 'success') {
        // this.global.presentLoadingClose();
        setTimeout(() => {
          this.loading = false;
          this.payoutRequestArr = res?.data?.requests;
        //  this.payoutRequestArr = [
        //   {
        //     "id": 2,
        //     "user_id": 3,
        //     "code": "APZ-8993645482",
        //     "wallet_type": "mainwallet",
        //     "amount": 50,
        //     "remarks": "Test Purpose",
        //     "status": "pending",
        //     "adminremarks": null,
        //     "type": "payout",
        //     "transaction_copy": null,
        //     "created_at": "01 May 22 - 01:03 AM",
        //     "updated_at": "01 May 22 - 01:03 AM",
        //     "transaction_copy_path": null
        //   },
        //   {
        //     "id": 1,
        //     "user_id": 3,
        //     "code": "APZ-1677197492",
        //     "wallet_type": "mainwallet",
        //     "amount": 50,
        //     "remarks": "Test Purpose",
        //     "status": "approved",
        //     "adminremarks": "TxnID : TTTTT10011524154",
        //     "type": "payout",
        //     "transaction_copy": null,
        //     "created_at": "16 Apr 22 - 02:36 AM",
        //     "updated_at": "16 Apr 22 - 02:46 AM",
        //     "transaction_copy_path": null
        //   }
        // ]
          if (event != null) { event.target.complete(); }
          //this.global.presentToast(res.msg);
        }, 200)
      }else {
        this.global.presentToast(res.msg);
        // this.global.presentLoadingClose();
        setTimeout(() => {
          this.loading = false;
          this.payoutRequestArr = [];
          if (event != null) { event.target.complete(); }
        }, 200)
      }
    })
  }

  doRefresh(event) {
    this.getList(event)
  }

}
