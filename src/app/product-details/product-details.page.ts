import { Component, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';
import { IonContent, NavController } from '@ionic/angular';
// import { PhotoViewer } from '@awesome-cordova-plugins/photo-viewer/ngx';
@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.page.html',
  styleUrls: ['./product-details.page.scss'],
})
export class ProductDetailsPage implements OnInit {
  @ViewChild(IonContent, { static: false }) content: IonContent;
  public showIncDescBtn: boolean = false;

  public sizeOrVariant: any;
  public productColorCode: any;


  public productId: any = "";
  public productFullData: any = {};
  public visableProduct: any = {}
  public others_products: any = [];
  public totalList:any = [];
  public defaultImage: any = 'https://miro.medium.com/max/441/1*9EBHIOzhE1XfMYoKz1JcsQ.gif';
  pageSource:any = ""
  constructor(
    private route: Router,
    public activeRoute: ActivatedRoute,
    public global: GlobalService,
    public apiService: ApiService,
    private sanitizer: DomSanitizer,
    public navCtrl: NavController,
    // private photoViewer: PhotoViewer
  ) { 
    this.activeRoute.queryParams.subscribe(params => {
      console.log("params : ", params)
      this.productId = params["product_id"];
      this.totalList = JSON.parse(params["others_products"]);
      this.pageSource = params["from"] == 'home'?params["from"]:"";
      if (this.productId) {
        this.global.presentLoadingDefault();
        this.fetchProductDetails("starting");
        //console.log(this.others_products)
      }
      setTimeout(()=>{
        this.others_products = []
        if (this.totalList && this.productId) {
          for (let i = 0; i < this.totalList.length; i++) {
            if (this.totalList[i].id != this.productId) {
              this.others_products.push(this.totalList[i]);
            }
          }
        }
      },1500)
    
      // console.log(this.others_products)
      
    })

  }

  ngOnInit() {
    
  }

  ionViewWillEnter() {
   
  }

  gotoCart() {
    this.route.navigate(['./product-checkout']);
  }

  gotoProductDetails(item) {
    console.log(item)
    this.productId = item?.id;
    let arr = this.totalList;
    this.others_products = [];
    for (let j = 0; j < arr.length; j++) {
      if (arr[j].id != this.productId) {
        this.others_products.push(arr[j]);
      }
    }
    console.log(this.others_products)
    this.global.presentLoadingDefault();
    this.fetchProductDetails("starting");

  }



  previewPhoto(url) {
    console.log(url)
    //this.photoViewer.show(url, this.productFullData?.name);
  }

  fetchProductDetails(loadValue) {
    
    let paramData = {
      product_id: this.productId
    }
    this.apiService.loadProductDetails(paramData)
      .then((success: any) => {
        
        if (success.status == 'success') {
          if (success.data) {
            this.global.presentLoadingClose()
            console.log("details page data : ", success.data);
            this.productFullData = success?.data?.product;
            this.productFullData.is_favourite = true;
            
            if (this.productFullData) {
              if (loadValue == "starting") {
                this.currentVisableProduct()
              }
            }
            // if(this.homePageAllData){
            //   this.seperateSectionsFromAlData()
            // }
          }
        } else {
          this.global.presentLoadingClose()
          this.global.presentToast(success.message);
        }
        console.log("Product details  : ", success)
      })
      .catch((error: any) => {
        this.global.presentLoadingClose()
        console.log("Login error : ", error)
      })
  }

  //Add to favourite function
  favFunction(value) {

    this.wishlistAddRemove(value)
  }

  wishlistAddRemove(value) {
    this.global.presentLoadingDefault()
    let data = {
      product_id: this.visableProduct?.product_id
    }
    this.apiService.apiWithTokenBody(data, 'wishlist/submit')
      .then((success: any) => {
        if (success.status == 'success') {
          console.log(success)
          this.productFullData.is_wishlist = value;
          console.log(this.productFullData.is_wishlist)
          this.global.presentToast(success.message);
          this.global.presentLoadingClose()
        } else {
          this.global.presentToast(success.message);
          this.global.presentLoadingClose()

        }

      }).catch((err: any) => {
        console.log(err);
         this.global.presentLoadingClose()
        this.global.presentToast(err.message);

      })
  }

  currentVisableProduct() {
    if (this.productFullData?.product_variants) {
      this.visableProduct = this.productFullData?.product_variants[0];
      this.productColorCode = this.productFullData?.product_variants[0]?.color;
      this.sizeOrVariant = this.productFullData?.product_variants[0]?.variant_name;
      console.log("data====>",this.visableProduct)
      this.content.scrollToTop(1500);
    }

  }


  public changeSizeOrVariant(product) {
    console.log(product)
    if (this.sizeOrVariant != product) {
      this.sizeOrVariant = product;
      this.findProductVariantDetails(this.productColorCode, this.sizeOrVariant);
    }

  }

  changeProductColor(product) {
    if (this.productColorCode != product) {
      this.productColorCode = product;
      this.findProductVariantDetails(this.productColorCode, this.sizeOrVariant);
    }
  }


  coverNumber(data){
    return parseFloat(data)
  }


  findProductVariantDetails(productColorCode, variant) {
    this.global.presentLoadingDefault()
    let createVariant: any = this.productFullData?.variant ? this.productFullData.variant + ':' + variant : null
    let paramData = {
      product_id: this.productFullData?.id,
      color: productColorCode,
      variant: createVariant,
      // variant:"size:FREE",
    }
    console.log("paramdata : ", paramData);
    this.apiService.fetchProductVariantDetails(paramData)
      .then((success: any) => {
        this.global.presentLoadingClose()
        if (success.status == 'success') {
          if (success.data) {
            if (success.data.variant_details) {
              this.visableProduct = success.data.variant_details
              console.log( this.visableProduct)
              this.productColorCode = this.visableProduct?.color;
              this.sizeOrVariant = this.visableProduct?.variant_name;
              console.log(this.productColorCode, this.sizeOrVariant);
            }
          }
        } else {
          this.global.presentToast(success.message);
        }
        console.log("Product details  : ", success)
      })
      .catch((error: any) => {
        this.global.presentLoadingClose()
        console.log("Login error : ", error)
      })
  }


  addBulkQuantiry(item:any){
    console.log("working ....")
    this.global.alertController.create({
      header: 'Enter Quantity',
      message: 'Enter zero (0) to remove from cart',
      inputs: [
      {
          name: 'Quantity',
          type: 'number',
          placeholder: 'Enter Quantity',
          cssClass: 'change_design',
          // value:item?.product_variants[0]?.cart_quantity
          value:item?.cart_quantity
      }],
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Let me think');
          }
        },
        {
          text: 'Add',
          handler: (data) => {
            console.log("data : ", data.Quantity);
            if(data.Quantity > 99){
              this.global.presentToast("Quantity can not be grater than 99")
            }else{
              let count:number;
              // if(item?.product_variants[0]?.cart_quantity > data.Quantity){
              if(item?.cart_quantity > data.Quantity){
                // count = -(item?.product_variants[0]?.cart_quantity - data.Quantity)
                count = -(item?.cart_quantity - data.Quantity)
              }else{
                // count = data.Quantity - item?.product_variants[0]?.cart_quantity;
                count = data.Quantity - item?.cart_quantity;
              }
              console.log("working : ", count);
              this.addToCart(item, count)
              // this.addToCart(item, data.Quantity)
            }
          }
        }
      ]
    }).then(res => {
      res.present();
    });
  }


  public addToCart(item, value) {
    // console.log(item)
    // console.log(value)
    this.global.presentLoadingDefault();
    let paramData = {
      variant_id: item?.id,
      quantity: value
    }
    this.apiService.addToCart(paramData)
      // this.apiService.findProductList(paramData)
      .then((success: any) => {
        // this.global.presentLoadingClose();
        console.log("Cart msg : ", success)
        if (success.status == 'success') {
          this.global.presentToast(success.message);
          if (success.data) {
            this.global.globalCartCount = success.data.cartcount;
            this.global.globalCartPrice = success.data.carttotal;

            this.visableProduct = success?.data?.variant_details;
            this.productColorCode = success?.data?.variant_details?.color;
            this.sizeOrVariant = success?.data?.variant_details?.variant_name;

            this.fetchProductDetails(null);
          }
          // this.loadHomepageData();
        } else {
          this.global.presentLoadingClose()
          this.global.presentToast(success.message);
        }

      })
      .catch((error: any) => {
        this.global.presentLoadingClose()
        console.log("Login error : ", error)
      })


  }











  // public addToCart(){
  //   this.showIncDescBtn = !this.showIncDescBtn;
  // }




}
