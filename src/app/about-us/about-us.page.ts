import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';
@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.page.html',
  styleUrls: ['./about-us.page.scss'],
})
export class AboutUsPage implements OnInit {
  content:any;
  loading:boolean = true;
  defaultImage = 'https://miro.medium.com/max/441/1*9EBHIOzhE1XfMYoKz1JcsQ.gif';
  
  constructor(private route: Router,public global: GlobalService,
    private apiService: ApiService,) { 
      
    }

  ngOnInit() {
    this.getData()
  }

  

  getData() {
    this.apiService.apiWithoutToken({slug:'about-us'},'cms/contents')
      .then((success: any) => {
        if (success.status == 'success') {
          console.log(success)
           this.content = success?.data?.cmscontent?.content;
           console.log(this.content)
          setTimeout(() => {
            this.loading = false
          }, 1000)
        } else {
          this.global.presentToast(success.message);
          setTimeout(() => {
            this.loading = false
          }, 1000)
        }

      }).catch((err: any) => {
        console.log(err);
        this.global.presentToast(err.message);
        setTimeout(() => {
          this.loading = false
        }, 1000)
      })
  }

  openPage(page:any){
    this.route.navigate(['./'+page]);
  }

  open(link:any){
    // if(link == "website"){
    //   let openLink = 'https://arambaghplaza.com/'
    //   window.open(openLink,'_blank')
    // }else if(link == "website"){
    //   let openLink = 'https://m.facebook.com/Arambaghplazacom-104882800860206/';
    //   window.open(openLink,'_blank')
    // }else if(link == "website"){
    //   let openLink = 'https://api.whatsapp.com/send?phone=+917365833384&text=*Hi Team Arambagh Plaza,* I would like to talk with your expert';
    //   window.open(openLink,'_blank')
    // }
    window.open(link,'_blank')
  }
}
