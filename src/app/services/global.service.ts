import { Injectable } from '@angular/core';
import { AlertController, LoadingController, Platform, ToastController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppVersion } from '@awesome-cordova-plugins/app-version/ngx';
import { OneSignal } from '@awesome-cordova-plugins/onesignal/ngx';
import { Geolocation } from '@awesome-cordova-plugins/geolocation/ngx';

declare let google: any;

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  loading: any;
  loading2:any;
  isLoggedIn: boolean;
  user_token:any= ""
  userdetails: any = {};
  selectedScheme:any = ""
  user_defaultAddress:any={};
  store_details:any;
  selectedCategory:any = ""

  deviceToken: any = '';

  public globalCartCount:any=0;
  public globalCartPrice:any="";

  public defaultAddress:any={};
  public defaultAddress_id:any="";

  public appversionValue:any="";

  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    public alertController: AlertController,
    public http: HttpClient,
    private geolocation: Geolocation,
    private oneSignal: OneSignal,
    public appVersion: AppVersion,
    public appPlatform:Platform
  ) { }

  checkObjEmpty(obj:any) {
    if (Object.keys(obj).length === 0)
      return true;
    else
      return false;
  }

  isvalidphoneandEmailFormat(input:any) {
    var re = /^(?:\d{10}|\w+@\w+\.\w{2,3})$/;
    return re.test(input);
  }

  isvalidEmailFormat(email:any) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  async presentLoadingDefault() {
    this.loading = await this.loadingCtrl.create({
      message: '',
      cssClass: 'loader-waiting'
    });
    await this.loading.present();
  }

  presentLoadingClose() {
    this.loading.dismiss();
  }


  async imageLoading() {
   this.loading = await this.loadingCtrl.create({ 
      spinner: null,
      message: `<img class="loading" width="120px" height="120px" src="../../assets/imgs/scooter_ride.gif" />`,
      translucent: true,
      cssClass: 'my-custom-class',
      backdropDismiss: true
    });
     await this. loading.present();
  }

 

  async presentToast(txt: any) {
    const toast = await this.toastCtrl.create({
      message: txt,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  async presentToast_lazy(txt: any) {
    const toast = await this.toastCtrl.create({
      message: txt,
      duration: 5000,
      position: 'top'
    });
    toast.present();
  }


  tomorrow() {
    let today = new Date();
    let todayDate = ('0' + (today.getDate() + 1)).slice(-2);
    let todayMonth = ('0' + (today.getMonth() + 1)).slice(-2);
    let year = today.getFullYear();
    let mindate = year + '-' + todayMonth + '-' + todayDate;
    return (mindate);
  }

  maxday() {
    let today = new Date();
    let todayDate = ('0' + (today.getDate() + 1)).slice(-2);
    let todayMonth = ('0' + (today.getMonth() + 1)).slice(-2);
    let lstyear = today.getFullYear() + 10;
    let maxdate = lstyear + '-' + todayMonth + '-' + todayDate;
    return (maxdate);
  }


  getLatLng() {
    this.geolocation.getCurrentPosition().then((resp:any) => {
      console.log(resp)
      let data = {
        lat: resp.coords.latitude,
        lng: resp.coords.longitude
      }
      return data

    }).catch((error:any) => {
      console.log('Error getting location', error);
    });
  }

  getAddress(lat: any, lng: any) {
    return new Promise(resolve => {
      // this.http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + lng + '&key=AIzaSyB8xbx_iVUi3F7lI4CSPOU11gZiI1hV4Zk')
      this.http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + lng + '&key=AIzaSyAqn3Jbfd49oAW6ghNhrbYniLcs0p332xg')

      
        .subscribe((data: any) => {
          console.log(data);
          let dataobject = data;
          let addressObj = {
            lat: lat,
            lng: lng,
            addr: dataobject.results[0].formatted_address
          }
          resolve(addressObj);
        })
    })
  }

  getAddressobj(lat: any, lng: any) {
    return new Promise(resolve => {
      // this.http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + lng + '&key=AIzaSyB8xbx_iVUi3F7lI4CSPOU11gZiI1hV4Zk')
      this.http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + lng + '&key=AIzaSyAqn3Jbfd49oAW6ghNhrbYniLcs0p332xg')
        .subscribe((data: any) => {
          if (data) {
            console.log(data);
            resolve(data);
          }
        })
    })
  }


  geoCode(address: any): Promise<any> {
    return new Promise((success) => {
      let geocoder = new google.maps.Geocoder();
      geocoder.geocode({ 'address': address }, (results:any, status:any) => {
        // console.log(results, results[0].formatted_address,results[0].geometry.location.lat(), results[0].geometry.location.lat())
        success(results[0]);
      })
    })
  }


  splitBySpace(str:any) {
    return str.split(' ')[0];
  }

  // async presentAlert(msg) {
  //   const alert = await this.alertController.create({
  //     cssClass: 'my-custom-class',
  //     // header: 'Alert',
  //     // subHeader: 'Subtitle',
  //     mode: 'ios',
  //     message: msg,
  //     buttons: ['OK']
  //   });

  //   await alert.present();
  // }

  async presentAlert(msg: any): Promise<any> {
    return new Promise((success) => {
      this.alertController.create({
        cssClass: 'my-custom-class',
        // header: 'Alert',
        // subHeader: 'Subtitle',
        mode: 'ios',
        message: msg,
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              success(1);
            }
          }
        ]
      })
        .then(alert => {
          // Now we just need to present the alert
          alert.present();
        });
    });
  }

  async versionAlert(msg: any): Promise<any> {
    return new Promise((success) => {
      this.alertController.create({
        cssClass: 'my-custom-class',
        mode: 'ios',
        header:"Update",
        message: msg,
        backdropDismiss: false,
        buttons: [
          {
            text: 'Cancel',
            handler: () => {
              success(1);
            }
          },
          {
            text: 'Update',
            handler: () => {
              success(2);
            }
          }
        ]
      })
        .then(alert => {
          // Now we just need to present the alert
          alert.present();
        });
    });
  }

  setPushNotification() {
    console.log('inside set push notification');
    //this.oneSignal.startInit('64dbe40a-c44a-4c89-9cd5-2491e98f1679', '549380438933');
    this.oneSignal.startInit('a1ca9756-a0ff-4fd7-b92c-69450b6d88ec', '398644667795');

    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
    this.oneSignal.handleNotificationReceived().subscribe((notificationData:any) => {
      // do something when notification is received
      console.log("Notification Data ==>", notificationData);
    });
    this.oneSignal.handleNotificationOpened().subscribe((notificationData: any) => {
      // do something when a notification is opened
      console.log(notificationData);
    });
    this.oneSignal.endInit();

    this.oneSignal.getIds().then((identity:any) => {
      console.log(identity);
      this.deviceToken = identity.userId;
    });
  }

}

