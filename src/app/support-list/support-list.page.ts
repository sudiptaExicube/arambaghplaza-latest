import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuController, NavController } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-support-list',
  templateUrl: './support-list.page.html',
  styleUrls: ['./support-list.page.scss'],
})
export class SupportListPage implements OnInit {
  loading:boolean = true;
  supprtArr:any;
  constructor(
    private route: Router,
    public global: GlobalService,
    private menu: MenuController,
    private apiService: ApiService,
    public activeRoute: ActivatedRoute,
    public navCtrl:NavController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getAllSupport(null)
  }


  getAllSupport(event){
    //this.global.presentLoadingDefault();
    this.apiService.apiWithToken('support-ticket/fetch')
    .then((success:any)=>{
      console.log(success)
      if(success.status == 'success'){
        console.log(success)
        this.supprtArr = success?.data?.tickets;
        if (event != null) {event.target.complete(); }
        setTimeout(()=>{
          this.loading=false
        },1000)
      }else{
        this.global.presentToast(success.message);
        if (event != null) {event.target.complete(); }
        setTimeout(()=>{
          this.loading=false
        },1000)
      }
     
    }).catch((err: any) => {
      console.log(err);
      this.global.presentToast(err.message);
      if (event != null) {event.target.complete(); }
      setTimeout(()=>{
        this.loading=false
      },1000)
     
    })
  }

  doRefresh(event){
    this.getAllSupport(event)
  }

  addNew(){
    this.route.navigate(['./support']);
  }

}
