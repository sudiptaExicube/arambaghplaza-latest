import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SuccessfullScreenPageRoutingModule } from './successfull-screen-routing.module';

import { SuccessfullScreenPage } from './successfull-screen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SuccessfullScreenPageRoutingModule
  ],
  declarations: [SuccessfullScreenPage]
})
export class SuccessfullScreenPageModule {}
